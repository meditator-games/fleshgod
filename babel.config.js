module.exports = api => {
  if (api) {
    api.cache.forever();
  }
  return {
    presets: [
      ["@babel/preset-env", {
        useBuiltIns: 'usage'
      }]],
    plugins: [
      '@babel/plugin-transform-runtime',
      '@babel/plugin-syntax-dynamic-import',
      '@babel/plugin-syntax-import-meta',
      ['@babel/plugin-proposal-class-properties', {loose: false }],
      '@babel/plugin-proposal-json-strings',
      ['@babel/plugin-transform-react-jsx', {
        pragma: 'createElement'
      }]
    ]
  };
};
