#!/bin/bash

for sndfile in *.wav; do
  lame $sndfile ../../resources/snd/$(basename -s .wav $sndfile).mp3
done
