#!/bin/bash

PNAME=fleshgod
SRCDIR="$(cd "$(dirname "$0")"; pwd -P)"
BUILDDIR="$SRCDIR/.dist"
ARTIFACTDIR="$SRCDIR/artifacts"
ZIPARC="$ARTIFACTDIR/$PNAME.zip"
ITCHPROJ="cyclopsian/$PNAME:browser"

set -e

deploy_rsync() {
  if [ -e dist/bundle.js.map ]; then
    make clean_webpack
  fi
  make ENV=production
  if which optipng; then
    make gfx_opt
  fi
  local url="$1"
  local islocal
  if [[ "$1" != *":"* ]]; then
    url=$(readlink -f "$1")
    islocal=1
    mkdir -p "$url"
  fi
  cd $SRCDIR/public
  rsync -avu . "$url"
  cd $SRCDIR/dist
  rsync -avu . "$url"
  if [ -n "$islocal" ]; then
    cd "$url"
  fi
}

deploy_gh() {
  mkdir -p "$BUILDDIR"
  cd "$BUILDDIR"
  if [ ! -d .git ]; then
    git init
    if [ -z "$2" ]; then
      echo "must pass git remote as second argument during repo init" > /dev/stderr
      exit 1
    fi
    git remote add origin "$1"
  else
    if [ -n "$1" -a "$1" != --amend ]; then
      git remote set-url origin "$1"
    fi
  fi
  deploy_rsync "$BUILDDIR"
  git add -A
  local commitargs=$([ "$1" == --amend ] && echo "$1")
  git commit $commitargs
  git push origin gh-pages -f
}

deploy_itch() {
  make ENV=production zip
  butler push "$ZIPARC" "$ITCHPROJ"
}

deploy_remote() {
  deploy_rsync "$BUILDDIR"
  rsync --delete --exclude .git/ -avu . "$1"
}

case "$1" in
  github | gh)
    deploy_gh "$2"
    ;;
  rsync)
    deploy_rsync "$2"
    ;;
  remote)
    deploy_remote "$2"
    ;;
  itch)
    deploy_itch
    ;;
  *)
    echo "usage: $0 command
  where command is:
    github, gh, itch, rsync, remote" > /dev/stderr
    exit 1
    ;;
esac
