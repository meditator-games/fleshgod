
import { createCanvas, loadImage } from 'canvas';
import { promises as fs } from 'fs';
import path from 'path';

const IMAGES = Object.freeze([
  [{filtering: false},
    'title', 'powerup', 'particle',
    'player', 'player-shot',
    'enemy', 'enemy-shot',
    'boss', 'eyeball',
    'explosion-1', 'explosion-2', 'explosion-3', 'explosion-4', 'explosion-5',
    'explosion-6', 'explosion-7', 'explosion-8', 'explosion-9',
    'meatball-1', 'meatball-2', 'meatball-3'
    ]
]);

function loadImages(basedir, files) {
  return files.map((names, id) => {
    const group = {id, images: [], filtering: true};
    for (const mem of names) {
      if (typeof mem === 'string') {
        const filename = `${basedir}/${mem}.png`;
        group.images.push(loadImage(filename)
          .then(image => {
            image.name = mem;
            return image;
          })
          .catch(e => Promise.reject(`Couldn't open ${filename}`)));
      } else if (mem && typeof mem === 'object') {
        Object.assign(group, mem);
      }
    }
    return group;
  });
}

function lowerBound(value, array, comp = (a, b) => a < b) {
  let first = 0;
  let index = 0;
  let count = array.length;
  let step = 0;

  while (count > 0) {
    index = first;
    step = count >> 1;
    index += step;
    if (comp(array[index], value)) {
      first = ++index;
      count -= step + 1;
    } else {
      count = step;
    }
  }
  return first;
}

function tryCreateAtlas(csize, images) {
  const canvas = createCanvas(csize, csize);
  const ctx = canvas.getContext('2d');
  const lines = [];
  const compSpan = ([a], [b]) => a < b;
  const rects = {};
  const pos = {x:0, y:0};
  const bounds = {x:0, y:0};
  const testCollide = image => {
    for (let yi = 0; yi < image.naturalHeight; yi++) {
      const row = lines[pos.y + yi] = lines[pos.y + yi] || [];
      for (const [sxa, sxb] of row) {
        if (pos.x < sxb && pos.x + image.naturalWidth > sxa) {
          return true;
        }
      }
    }
    return false;
  };
  images = images.slice();
  images.sort((a, b) => b.naturalWidth * b.naturalHeight - a.naturalWidth * a.naturalHeight);
  let step =0;
  for (const image of images) {
    const size = {x:image.naturalWidth, y:image.naturalHeight};
    for(;;) {
      if (pos.y + size.y > ctx.canvas.height) {
        return null;
      }
      if (testCollide(image)) {
        const row = lines[pos.y] || [];
        const closest = lowerBound([pos.x], row, compSpan);
        if (closest >= row.length) { // skip to next row if nothing left to jump to
          pos.x = 0;
          pos.y += 1;
        } else { // skip past width of the next span
          pos.x = row[closest][1];
          if (pos.x + size.x > ctx.canvas.width) { // make sure we don't hit the right edge
            pos.x = 0;
            pos.y += 1;
          }
        }
      } else {
        break;
      }
    }
    ctx.drawImage(image, pos.x, pos.y);
    rects[image.name] = {pos: [pos.x, pos.y], size: [size.x, size.y]};
    bounds.x = Math.max(pos.x + size.x, bounds.x);
    bounds.y = Math.max(pos.y + size.y, bounds.y);
    for (let yi = 0; yi < size.y + 1; yi++) { // add 1 to pad it out
      const row = lines[pos.y + yi] = lines[pos.y + yi] || [];
      const span = [pos.x, pos.x + size.x + 1]; // add 1 to pad it out
      row.splice(lowerBound(span, row, compSpan), 0, span); // keep it sorted
    }
    /*
    const dimg = createCanvas(csize, csize);
    const dc = dimg.getContext('2d');
    let cstep = 255;
    for (let y = 0; y < lines.length; y++) {
      for (const [sxa, sxb] of (lines[y] || [])) {
        dc.fillStyle = `rgb(0, ${cstep}, 0)`;
        dc.fillRect(sxa, y, sxb - sxa, 1);
        cstep--;
        if (cstep < 0) cstep = 255;
      }
    }
    fs.writeFile(`${outdir}/debug${step}.png`, dimg.toBuffer());
    console.log(`${outdir}/debug${step}.png`);
    step++;
    */
  }
  const cropped = createCanvas(bounds.x, bounds.y);
  cropped.getContext('2d').drawImage(canvas, 0, 0);
  return {canvas: cropped, rects};
}

const MAX_TEXTURE_SIZE = 2048;

function createAtlas({id, images, filtering}) {
  let minPixels = 0;
  let minSize = 0;
  for (const img of images) {
    minPixels += img.naturalWidth * img.naturalHeight;
    minSize = Math.max(minSize, img.naturalWidth, img.naturalHeight);
  }
  let textureSize = Math.pow(2, Math.ceil(
    Math.log2(Math.max(minSize, Math.sqrt(minPixels)))
  ));
  let atlas = null;
  while (!atlas && textureSize <= MAX_TEXTURE_SIZE) {
    atlas = tryCreateAtlas(textureSize, images);
    if (!atlas) textureSize *= 2;
  }
  if (!atlas) {
    throw new Error(`Texture atlas ${id} exceeds maximum texture size ${MAX_TEXTURE_SIZE}`);
  }
  atlas.id = id;
  atlas.filtering = filtering;
  return atlas;
}

const [, cmd, basedir, outdir] = process.argv;
if (process.argv.length < 4) {
  console.error([`usage: node ${cmd} basedir outdir\n`,
    '  build texture atlases from images in basedir into outdir'].join('\n'));
  process.exit(1);
}

(async () => {
  const atlases = await Promise.all(
    loadImages(basedir, IMAGES).map(async (group) => {
      group.images = await Promise.all(group.images);
      const atlas = createAtlas(group);
      const filename = path.join(outdir, `${atlas.id}.png`);
      await fs.writeFile(filename, atlas.canvas.toBuffer());
      console.log(filename);
      return atlas;
    }));
  const config = atlases.map(({id, rects, filtering}) => ({id, rects, filtering}));
  await fs.writeFile(path.join(__dirname, 'atlases.json'), JSON.stringify(config));
})();
