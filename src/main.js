
import { SCREEN_SIZE, TICK_LENGTH } from './defs.js';
import CanvasBackend from './canvas-backend.js';
import Game from './game.js';
import Renderer from './render.js';
import V2 from './util/vector2.js';
import {error} from './debug.js';
import localForage from 'localforage';

export default function main(
  { audio, config, window, canvas, gfx, sounds, ui, GLBackend }) {
  const bound = new Set();
  const system = new System({window, bound, ui});
  system.setup(canvas, ui);
  const game = new Game({window, canvas, system, sounds, audio, config, ui});
  [...Object.keys(game.keybinds)].forEach(k => bound.add(k));
  const renderer = new Renderer({game, system, config, now: window.performance.now()});

  Object.assign(ui, {
    system,
    createBackend (config, canvas) {
      if (GLBackend) {
        const gl = config.gl && canvas.getContext('webgl', { depth: false, alpha: false, antialias: false });
        if (gl) return new GLBackend(gl, config);
      }
      return new CanvasBackend(canvas.getContext('2d', { antialias: false }));
    },
    save() {
      return localForage.setItem('fleshgodSettings', JSON.stringify(config))
        .catch(err => error(err));
    },
    redraw() {
      const now = window.performance.now();
      renderer.frame(ctx, now);
      /*
         renderer.nextFrameTimes.push(window.performance.now() - now);
         if (renderer.nextFrameTimes.length >= 10) {
         renderer.frameTimes = renderer.nextFrameTimes;
         renderer.nextFrameTimes = [];
         }
         */
      // debug(`FT: ${Math.round(() * 1000)}us`);
    },
    reloadCanvas() {
      const oldCanvas = canvas;
      canvas = window.document.createElement('canvas');
      canvas.width = oldCanvas.width;
      canvas.height = oldCanvas.height;
      canvas.id = oldCanvas.id;
      system.setup(canvas, this);
      setupCanvas();
      game.canvas = canvas;
      oldCanvas.parentElement.replaceChild(canvas, oldCanvas);
      ctx = this.createBackend(config, canvas);
      ctx.setup(gfx);
      this.redraw();
    },
    project(pos) {
      return game.projectCoord(pos);
    },
    reloadSound() {
      game.tickSound();
    }
  });

  canvas.width = SCREEN_SIZE.x;
  canvas.height = SCREEN_SIZE.y;
  let ctx = ui.createBackend(config, canvas);
  ctx.setup(gfx);
  ui.resized();

  const eventCb = (type, code) => () => system.events.push({ type, code });
  // 'Vendor: 054c Product: 09cc': { // PS4 Pad
  // 'Vendor: 045e Product: 02ea': { // XBONE Pad
  const STANDARD_BINDS = Object.freeze({
    axis0: ({value}) => system.events.push({ type: 'movex', value }),
    axis1: ({value}) => system.events.push({ type: 'movey', value }),
    axis2: ({value}) => system.events.push({ type: 'lookx', value }),
    axis3: ({value}) => system.events.push({ type: 'looky', value }),
    'button12down': eventCb('keydown', 'ArrowUp'),
    'button13down': eventCb('keydown', 'ArrowDown'),
    'button14down': eventCb('keydown', 'ArrowLeft'),
    'button15down': eventCb('keydown', 'ArrowRight'),
    'button12up': eventCb('keyup', 'ArrowUp'),
    'button13up': eventCb('keyup', 'ArrowDown'),
    'button14up': eventCb('keyup', 'ArrowLeft'),
    'button15up': eventCb('keyup', 'ArrowRight'),
    'button0down': eventCb('keydown', 'Space'),
    'button1down': eventCb('keydown', 'KeyQ'),
    'button0up': eventCb('keyup', 'Space'),
    'button1up': eventCb('keyup', 'KeyQ'),
    'button9down': eventCb('keydown', 'Enter')
  });
  const GAMEPAD_BINDS = Object.freeze({
    'Vendor: 1d57 Product: 0020': { // SNES Pad
      axis0: ({value}) => system.events.push({ type: 'movex', value }),
      axis1: ({value}) => system.events.push({ type: 'movey', value }),
      'button1down': eventCb('keyup', 'KeyQ'),
      'button0down': eventCb('keyup', 'Space')
    }
  });

  const setupCanvas = () => {
    canvas.addEventListener('contextmenu', e => {
      e.preventDefault();
      e.stopPropagation();
      return false;
    });
  };
  setupCanvas();
  setInterval(() => {
    if (ui.blocking()) {
      system.poll();
    } else {
      system.updateGamepads(STANDARD_BINDS, GAMEPAD_BINDS);
      game.tick();
    }
  }, TICK_LENGTH);
  /*
  setInterval(() => {
    localForage.setItem('board', JSON.stringify(game.save())).catch(err => error(err));
  }, 1000 / 1);
  */

  (function dispatchRender() {
    if (!ui.blocking()) {
      ui.redraw();
    }
    window.requestAnimationFrame(dispatchRender);
  }());
}

class System {
  static keyCode(e) {
    return [e.altKey ? 'Alt_' : '', e.ctrlKey ? 'Ctrl_' : '', e.metaKey ? 'Meta_' : '', e.code].join('');
  }
  constructor({window, bound, ui}) {
    this.events = [];
    this.mousePos = V2(NaN);
    this.window = window;
    this.padStates = new Map();
    this.padLastStates = new Map();
    this.padDeadZone = 0.5;
    this.touchEnabled = false;

    window.addEventListener('resize', () => ui.resized());
    window.addEventListener('mousemove', e => {
      if (ui.blocking()) return;
      this.events.push(e);
      this.mousePos = V2(e.clientX, e.clientY);
      this.mouseHover = e.target;
    });
    window.addEventListener('mouseup', e => {
      if (ui.blocking()) return;
      this.events.push(e);
    });
    window.addEventListener('touchend', e => {
      this.touchEnabled = true;
      if (ui.blocking()) return;
      this.events.push(e);
    }, { passive: false });
    window.addEventListener('touchmove', e => {
      this.touchEnabled = true;
      if (ui.blocking()) return;
      this.events.push(e);
      e.preventDefault();
      e.stopPropagation();
    }, { passive: false });
    window.document.body.addEventListener('keydown', e => {
      if (ui.blocking()) return;
      if (!e.repeat) {
        this.events.push(e);
      }
      if (bound.has(this.constructor.keyCode(e))) {
        e.preventDefault();
      }
    });
    window.document.body.addEventListener('keyup', e => {
      if (ui.blocking()) return;
      if (!e.repeat) {
        this.events.push(e);
      }
      if (bound.has(this.constructor.keyCode(e))) {
        e.preventDefault();
      }
    });
  }
  setup(canvas, ui) {
    canvas.addEventListener('mousedown', e => {
      if (ui.blocking()) return;
      this.events.push(e);
    });
    canvas.addEventListener('touchstart', e => {
      this.touchEnabled = true;
      if (ui.blocking()) return;
      const {changedTouches: touches} = e;
      if (touches.length) {
        this.events.push(e);
      }
    }, { passive: false });
  }
  updateGamepads(standard, binds) {
    const {window} = this;
    if (window.navigator && window.navigator.getGamepads) {
      const gamepads = [...window.navigator.getGamepads()].filter(v => v);
      for (const pad of gamepads) {
        if (pad.mapping === 'standard') {
          this.tickGamepad(pad, standard);
        } else {
          for (const [padName, pb] of Object.entries(binds)) {
            if (pad.id.match(new RegExp(padName, 'i'))) {
              this.tickGamepad(pad, pb);
              break;
            }
          }
        }
      }
    }
  }
  tickGamepad(pad, binds) {
    const {padStates, padLastStates} = this;
    const state = padStates.get(pad.id) || {};
    padStates.set(pad.id, state);
    const lastState = Object.assign({}, state);
    padLastStates.set(pad.id, lastState);
    pad.axes.forEach((value, index) => state[`axis${index}`] = value);
    pad.buttons.forEach(({pressed}, index) => state[`button${index}`] = pressed);
    for (const ev of Object.keys(state)) {
      if (state[ev] !== lastState[ev]) {
        if (ev.match('button')) {
          const e = { type: `${ev}${state[ev] ? 'down' : 'up'}` };
          (binds[e.type] || (() => 0))(e);
        } else if (ev.match('axis')) {
          const s = state[ev];
          const p = lastState[ev];
          const spressed = Math.abs(s) >= this.padDeadZone;
          const ppressed = Math.abs(p) >= this.padDeadZone;
          if (!ppressed && spressed) {
            const e = { type: ev + (s > 0 ? '+' : '-') + 'down' };
            (binds[e.type] || (() => 0))(e);
          } else if (ppressed && !spressed) {
            const e = { type: ev + (p > 0 ? '+' : '-') + 'up' };
            (binds[e.type] || (() => 0))(e);
          } else if (ppressed && spressed && (p > 0) !== (s > 0)) {
            const e = { type: ev + (p > 0 ? '+' : '-') + 'up' };
            (binds[e.type] || (() => 0))(e);
            const e2 = { type: ev + (s > 0 ? '+' : '-') + 'down' };
            (binds[e.type] || (() => 0))(e2);
          }
          (binds[ev] || (() => 0))({ type: ev, value: state[ev] });
        }
      }
    }
  }
  poll() {
    return this.events.splice(0, this.events.length);
  }
}

