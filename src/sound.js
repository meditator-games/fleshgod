
export { load, trigger, setVolume, stop };

function load(audio, buffer) {
  return new Promise((reject, resolve) =>
    audio.decodeAudioData(buffer, reject, resolve));
}

function stop(sound) {
  if (sound && sound.source) {
    if (sound.source.stop) sound.source.stop();
    sound.gainNode.disconnect();
    sound.source.disconnect();
    delete sound.source;
  }
}

function setVolume(sound, volume) {
  if (sound && sound.source) {
    sound.gainNode.gain.value = volume;
  }
}

function trigger({audio, now}, sound, {volume = 0.5, loop = null} = {}) {
  if (audio
      && (!sound.source
        || ((now - sound.startedAt) / 1000 >= (sound.retrigger || 0)))) {
    const play = () => {
      stop(sound);
      sound.startedAt = now;
      sound.source = audio.createBufferSource();
      sound.source.buffer = sound;
      sound.gainNode = audio.createGain();
      sound.gainNode.gain.value = volume;
      sound.source.connect(sound.gainNode);
      sound.gainNode.connect(audio.destination);
      if (loop) {
        sound.source.loop = true;
        if (typeof loop === 'number') {
          sound.source.loopStart = 0;
          sound.source.loopEnd = loop;
        }
      }
      sound.source.start();
    };
    play();
  }
}

