import COLOR_FRAG from './shaders/color.frag';
import COLOR_VERT from './shaders/color.vert';
import SCREEN_FRAG from './shaders/screen.frag';
import SCREEN_VERT from './shaders/screen.vert';
import HIGHLIGHTS_FRAG from './shaders/highlights.frag';
import HIGHLIGHTS_VERT from './shaders/highlights.vert';
import BLUR_FRAG from './shaders/blur.frag';
import BLUR_VERT from './shaders/blur.vert';
import UI_FRAG from './shaders/ui.frag';
import UI_VERT from './shaders/ui.vert';
import WORLD_FRAG from './shaders/world.frag';
import WORLD_VERT from './shaders/world.vert';
import OFFSCREEN_VERT from './shaders/offscreen.vert';
import GLASS_FRAG from './shaders/glass.frag';
import RIPPLE_FRAG from './shaders/ripple.frag';
import SINE_FRAG from './shaders/sine.frag';
import V2 from './util/vector2.js';
import { DEFAULT_FONT_FAMILY, DEFAULT_FONT_SIZE } from './defs.js';
import { debug, error } from './debug.js';
import { lowerBound } from './util/misc.js';
import { mat3, mat4, vec2, vec3 } from 'gl-matrix';

const OFFSCREEN_SIZE = 2048;

function hexToGL(color) {
  return [
    parseInt(color.substr(1, 2), 16) / 255,
    parseInt(color.substr(3, 2), 16) / 255,
    parseInt(color.substr(5, 2), 16) / 255
  ];
}

export default class GLBackend {
  constructor(gl, config) {
    this.alpha = 1;
    this.z = 0;
    this.gl = gl;
    this.config = config;
    this.canvas = gl.canvas;
    this.textCtx = gl.canvas.ownerDocument.createElement('canvas').getContext('2d');
    this.tris = 0;
    this.coloredQuads = [];
    this.uiTris = 2;
    this.boxes = [];
    this.lines = 0;
    this.textures = {}; // {[name]: { texture: WebGLTexture, uv: { a: V2, b: V2 }}}
    this.textures.empty = {texture: null, size: null, uva: V2(0, 0), uvb: V2(0, 0)};
    this.gameQuadGroups = new Map;
    this.uiQuadGroups = new Map;
    this.shaderArgs = new Map;
  }
  drawQuad(img, a, options = {}) {
    const {texture, size: texSize, uva, uvb} = this.textures[img];
    const { alpha = this.alpha, transforms = [], size = texSize, z = this.z, program = {type:'world'} } = options;
    const ex = [
      {uvx: uva.x, uvy: uva.y, l: 1},
      {uvx: uva.x, uvy: uvb.y, l: 1},
      {uvx: uvb.x, uvy: uvb.y, l: 1},
      {uvx: uvb.x, uvy: uva.y, l: 1}
    ];
    const vs = [
      a,
      V2(a.x, a.y + size.y),
      V2(a.x + size.x, a.y + size.y),
      V2(a.x + size.x, a.y)
    ];
    const transformFuncs = {
      flipX: () => [ex[0], ex[1], ex[2], ex[3]] = [ex[3], ex[2], ex[1], ex[0]],
      flipY: () => [ex[0], ex[1], ex[2], ex[3]] = [ex[1], ex[0], ex[3], ex[2]],
      scale: ({x, y}) => transformMat(mat3.fromScaling(mat3.create(), [x, y])),
      rotate: ({angle}) => transformMat(mat3.fromRotation(mat3.create(), angle)),
      matrix: ({mat}) => transformMat(mat)
    };
    let center;
    const transformMat = mat => {
      if (!center) {
        center = a.clone().add(size.x / 2, size.y / 2);
      }
      for (const v of vs) {
        const o = vec2.transformMat3(vec2.create(), vec2.fromValues(v.x - center.x, v.y - center.y), mat);
        v.x = center.x + o[0];
        v.y = center.y + o[1];
      }
    };
    if (transforms.length) {
      for (const transform of transforms) {
        if (typeof transform === 'string') {
          transformFuncs[transform]();
        } else {
          transformFuncs[transform.type](transform);
        }
      }
    }
    const {type: shaderType, ...uniforms} = program;
    const {screenSampler, screenTexBounds, ...defaults} = this.programs[shaderType].uniforms;
    let shader;
    const skey = JSON.stringify(Object.entries(program).sort(([a], [b]) => a - b));
    if (!screenTexBounds) {
      shader = this.shaderArgs.get(skey);
    }
    if (!shader) {
      shader = {
        program: this.programs[shaderType],
        uniforms: Object.assign(
          Object.entries(defaults).reduce((o, [name, {initial}]) => {
            if (typeof initial !== 'undefined' && initial !== null) {
              o[name] = initial;
            }
            return o;
          }, {}),
          uniforms
        )
      };
      if (!screenTexBounds) {
        this.shaderArgs.set(skey, shader);
      }
    }
    if (screenTexBounds) {
      let smin;
      let smax;
      for (let i = 0; i < vs.length; i++) {
        let v = vec3.fromValues(vs[i].x, vs[i].y, 0);
        vec3.transformMat4(v, v, this.view);
        vec3.transformMat4(v, v, this.projection);
        [ex[i].sx, ex[i].sy] = v = v.slice(0, 2)
          .map((v, i) => (v * 0.5 + 0.5) * this.fbScale[i % 2 ? 'y' : 'x']);
        if (!smin) {
          smin = smax = [v[0], v[1]];
        } else {
          smin = smin.map((s, k) => Math.min(s, v[k]));
          smax = smax.map((s, k) => Math.max(s, v[k]));
        }
      }
      shader.uniforms.screenTexBounds = new Float32Array([...smin, ...smax]);
    }
    const zGroup = this.gameQuadGroups.get(z) || new Map;
    if (!zGroup.size) this.gameQuadGroups.set(z, zGroup);
    const shaderGroup = zGroup.get(shader) || new Map;
    if (!shaderGroup.size) zGroup.set(shader, shaderGroup);
    const group = shaderGroup.get(texture) || [];
    if (!group.length) shaderGroup.set(texture, group);
    group.push({vs, ex, size, z, alpha});
    this.tris += 2;
    if (screenSampler) {
      this.tris += 2;
    }
  }
  drawUIQuad(img, a, options = {}) {
    const {texture, size: texSize, uva, uvb} = this.textures[img];
    const { alpha = this.alpha, size = texSize } = options;
    const group = this.uiQuadGroups.get(texture) || [];
    if (!group.length) this.uiQuadGroups.set(texture, group);
    const b = {x: a.x + size.x, y: a.y + size.y};
    group.push({a, b, alpha, ex: [
      {x: uva.x, y: uva.y },
      {x: uva.x, y: uvb.y },
      {x: uvb.x, y: uvb.y },
      {x: uvb.x, y: uva.y }
    ]});
    this.uiTris += 2;
  }
  measureText(text, size = DEFAULT_FONT_SIZE, family = DEFAULT_FONT_FAMILY) {
    const ctx = this.textCtx;
    ctx.font = `${size}px ${family}`;
    return ctx.measureText(text);
  }
  fillRect(pos, size, {color, z = 1, alpha=this.alpha}) {
    this.coloredQuads.push({a:pos, b:pos.clone().addV2(size), z, color: [...hexToGL(color), alpha] });
    this.uiTris += 2;
  }
  strokeRect(pos, size, {color, z = 1, alpha=this.alpha}) {
    this.boxes.push({a:pos.clone().add(0.5, 0.5), b:pos.clone().addV2(size).add(0.5, 0.5), z, color: [...hexToGL(color), alpha] });
    this.lines += 4;
  }
  drawText(text, { size = DEFAULT_FONT_SIZE, family = DEFAULT_FONT_FAMILY,
    y = 0, x = 0, z = 2, key = null, shadow = true, color='#ffffff' } = {}) {
    const {gl, textCtx: ctx} = this;
    const tk = key ? `k${key}` : `s${size}t${text}c${color}`;
    let texture = this.textTextures.get(tk);
    ctx.font = `${size}px ${family}`;
    const metrics = ctx.measureText(text);
    if (!texture || texture.lastText !== text || texture.lastHeight !== size || texture.lastWidth !== metrics.width) {
      if (!texture) {
        texture = gl.createTexture();
        this.textTextures.set(tk, texture);
        texture.groupId = 0xffff;
        gl.bindTexture(gl.TEXTURE_2D, texture);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
      } else {
        gl.bindTexture(gl.TEXTURE_2D, texture);
      }
      texture.lastText = text;
      texture.lastWidth = metrics.width;
      texture.lastHeight = size;
      ctx.canvas.width = metrics.width + 4;
      ctx.canvas.height = size * 1.5;
      ctx.font = `${size}px ${family}`;
      ctx.fillStyle = 'rgba(0, 0, 0, 0)';
      ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);
      if (shadow) {
        ctx.fillStyle = 'black';
        ctx.fillText(text, 4, size + 4);
      }
      ctx.fillStyle = color;
      ctx.fillText(text, 0, size);
      gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, ctx.canvas);
    }
    const group = this.uiQuadGroups.get(texture) || [];
    if (!group.length) this.uiQuadGroups.set(texture, group);
    group.push({
      a: V2(x, y - size),
      b: V2(x + metrics.width + 4, y + size * 0.5),
      z,
      alpha: 1,
      ex: [
        {x: 0, y: 0, l: 1 },
        {x: 0, y: 1, l: 1 },
        {x: 1, y: 1, l: 1 },
        {x: 1, y: 0, l: 1 }
      ]
    });
    this.uiTris += 2;
  }
  drawTextCentered(text, { size = DEFAULT_FONT_SIZE, family = DEFAULT_FONT_FAMILY,
    y = 0, x = 0, z = 2, key = null} = {}) {
    const {gl} = this;
    const metrics = this.measureText(text, size, family);
    this.drawText(text, {
      x: (gl.canvas.width - metrics.width) / 2 + x,
      y: (gl.canvas.height + size) / 2 + y,
      z,
      size,
      key
    });
  }
  setUniform({type, obj}, value) {
    if (type[0] === 'M') {
      this.gl[`uniform${type}`](obj, false, value);
    } else {
      this.gl[`uniform${type}`](obj, value);
    }
  }
  setup(gfx) {
    const {gl, canvas} = this;
    this.textTextures = new Map;

    const compile = (name, type, src) => {
      const shader = gl.createShader(type);
      gl.shaderSource(shader, src);
      gl.compileShader(shader);
      if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
        error(`${name}: ${gl.getShaderInfoLog(shader)}`);
        return null;
      }
      return shader;
    };
    const makeProgram = ({name, shaders, uniforms={}, attribs=[], defines={}}) => {
      const program = gl.createProgram();
      for (const [index, {type, src}] of Object.entries(shaders)) {
        gl.attachShader(program, compile(`${name} shader #${index}`, type,
          [...Object.entries(defines).map(([n, v]) => `#define ${n} ${v}`), src].join('\n')
        ));
      }
      gl.linkProgram(program);
      gl.validateProgram(program);
      if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
        error(`program ${name}: ${gl.getProgramInfoLog(program)}`);
      } else {
        program.uniforms = {};
        const uniformEntries = Object.entries(uniforms);
        if (uniformEntries.length || attribs.length) {
          gl.useProgram(program);
        }
        for (const [name, {type, initial}] of uniformEntries) {
          const uniform = program.uniforms[name] = {
            obj: gl.getUniformLocation(program, name),
            type,
            initial
          };
          if (typeof initial !== 'undefined' && initial !== null) {
            this.setUniform(uniform, initial);
          }
        }
        program.attribs = {};
        for (const attrib of attribs) {
          program.attribs[attrib] = gl.getAttribLocation(program, attrib);
        }
      }
      return program;
    };

    const viewport = this.viewport = V2(canvas.width, canvas.height);
    gl.viewport(0, 0, viewport.x, viewport.y);
    const proj = this.projection = mat4.ortho(mat4.create(), 0, viewport.x, viewport.y, 0, -4, 4);

    this.programs = {};
    this.programs.world = makeProgram({
      name: 'world',
      shaders: [
        { type: gl.VERTEX_SHADER, src: WORLD_VERT },
        { type: gl.FRAGMENT_SHADER, src: WORLD_FRAG }
      ],
      uniforms: {
        projection: { type: 'Matrix4fv', initial: proj },
        view: { type: 'Matrix4fv' },
        sampler: { type: '1i', initial: 0 },
      },
      attribs: ['position', 'extra']
    });

    gl.enableVertexAttribArray(this.programs.world.attribs.position);
    gl.enableVertexAttribArray(this.programs.world.attribs.extra);

    this.programs.color = makeProgram({
      name: 'color',
      shaders: [
        { type: gl.VERTEX_SHADER, src: COLOR_VERT },
        { type: gl.FRAGMENT_SHADER, src: COLOR_FRAG }
      ],
      uniforms: {
        projection: {type: 'Matrix4fv', initial: proj}
      },
      attribs: ['position', 'extra']
    });

    this.programs.ui = makeProgram({
      name: 'ui',
      shaders: [
        { type: gl.VERTEX_SHADER, src: UI_VERT },
        { type: gl.FRAGMENT_SHADER, src: UI_FRAG }
      ],
      uniforms: {
        projection: { type: 'Matrix4fv', initial: proj },
        sampler: { type: '1i', initial: 0 }
      },
      attribs: ['position', 'extra']
    });

    const fbSize = viewport.clone().map(v => Math.pow(2, Math.ceil(Math.log2(v))));
    this.fbSize = fbSize;
    this.fbScale = viewport.clone().divV2(fbSize);
    const pixelSize = V2(1, 1).divV2(fbSize);

    this.programs.highlights = makeProgram({
      name: 'highlights',
      shaders: [
        { type: gl.VERTEX_SHADER, src: HIGHLIGHTS_VERT },
        { type: gl.FRAGMENT_SHADER, src: HIGHLIGHTS_FRAG }
      ],
      uniforms: {
        fbScale: { type: '2fv', initial: new Float32Array(this.fbScale.array()) },
        fb: { type: '1i', initial: 0 }
      },
      attribs: ['position']
    });

    const SIGMA = 8;
    const KSIZE = 13;
    const SD = SIGMA * SIGMA * -2;

    const weights = new Float32Array(KSIZE);
    let sum = 0;
    for (let i = 1; i <= KSIZE; i++) {
      const w = Math.exp((i * i) / SD);
      weights[i - 1] = w;
      sum += w;
    }
    for (let i = 0; i < weights.length; i++) {
      weights[i] /= sum * 2;
    }
    this.programs.blur = makeProgram({
      name: 'blur',
      shaders: [
        { type: gl.VERTEX_SHADER, src: BLUR_VERT },
        { type: gl.FRAGMENT_SHADER, src: BLUR_FRAG }
      ],
      defines: {
        KSIZE
      },
      uniforms: {
        fbScale: { type: '2fv', initial: new Float32Array(this.fbScale.array()) },
        pixelSize: { type: '2fv', initial: new Float32Array(pixelSize.array()) },
        blurWeights: { type: '1fv', initial: weights },
        blurVector: { type: '2fv', initial: new Float32Array([1, 0]) },
        fb: { type: '1i', initial: 0 }
      },
      attribs: ['position']
    });


    this.programs.screen = makeProgram({
      name: 'screen',
      shaders: [
        { type: gl.VERTEX_SHADER, src: SCREEN_VERT },
        { type: gl.FRAGMENT_SHADER, src: SCREEN_FRAG }
      ],
      uniforms: {
        fbScale: { type: '2fv', initial: new Float32Array(this.fbScale.array()) },
        hilightsScale: { type: '2fv', initial: new Float32Array(this.fbScale.array()) },
        separation: { type: '2fv', initial: new Float32Array([0, 0, 0, 0, 0, 0]) },
        fb: { type: '1i', initial: 0 },
        hilights: { type: '1i', initial: 1 }
      },
      attribs: ['position']
    });

    this.programs.glass = makeProgram({
      name: 'glass',
      shaders: [
        { type: gl.VERTEX_SHADER, src: OFFSCREEN_VERT },
        { type: gl.FRAGMENT_SHADER, src: GLASS_FRAG }
      ],
      uniforms: {
        sampler: { type: '1i', initial: 0 },
        screenSampler: { type: '1i', initial: 1 },
        alpha: { type: '1f', initial: 1 },
        screenTexBounds: { type: '4fv' }
      },
      attribs: ['position', 'extra']
    });

    this.programs.vsine = makeProgram({
      name: 'vsine',
      shaders: [
        { type: gl.VERTEX_SHADER, src: WORLD_VERT },
        { type: gl.FRAGMENT_SHADER, src: SINE_FRAG }
      ],
      uniforms: {
        fbSize: { type: '2fv', initial: new Float32Array(fbSize.array()) },
        fbScale: { type: '2fv', initial: new Float32Array(this.fbScale.array()) },
        projection: { type: 'Matrix4fv', initial: proj },
        view: { type: 'Matrix4fv' },
        sampler: { type: '1i', initial: 0 },
        phase: { type: '1f', initial: 0 },
        screenTexBounds: { type: '4fv' }
      },
      attribs: ['position', 'extra']
    });

    this.programs.ripple = makeProgram({
      name: 'ripple',
      shaders: [
        { type: gl.VERTEX_SHADER, src: OFFSCREEN_VERT },
        { type: gl.FRAGMENT_SHADER, src: RIPPLE_FRAG }
      ],
      uniforms: {
        screenSampler: { type: '1i', initial: 1 },
        screenTexBounds: { type: '4fv' },
        thickness: { type: '1f', initial: 0.5 },
        depth: { type: '1f', initial: 1 },
        curve: { type: '1f', initial: 4 },
        transform: { type: 'Matrix3fv', initial: mat3.create() }
      },
      attribs: ['position', 'extra']
    });


    gl.activeTexture(gl.TEXTURE0);
    const maxTextureSize = gl.getParameter(gl.MAX_TEXTURE_SIZE);
    for (const atlas of gfx) {
      const texture = gl.createTexture();
      texture.groupId = atlas.id;
      gl.bindTexture(gl.TEXTURE_2D, texture);
      gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, atlas.image);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, atlas.filtering ? gl.LINEAR : gl.NEAREST);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, atlas.filtering ? gl.LINEAR : gl.NEAREST);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
      const textureSize = V2(atlas.image.naturalWidth, atlas.image.naturalHeight);
      for (const [name, {pos, size}] of Object.entries(atlas.rects)) {
        this.textures[name] = {
          texture,
          size: V2(...size),
          uva: V2(...pos).divV2(textureSize),
          uvb: V2(...pos).add(...size).divV2(textureSize)
        };
      }
    }

    this.triBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, this.triBuffer);
    this.triBufferSize = 1024 * 1024;
    gl.bufferData(gl.ARRAY_BUFFER, this.triBufferSize, gl.DYNAMIC_DRAW);

    this.lineBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, this.lineBuffer);
    this.lineBufferSize = 1024;
    gl.bufferData(gl.ARRAY_BUFFER, this.lineBufferSize, gl.DYNAMIC_DRAW);

    this.uiTriBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, this.uiTriBuffer);
    this.uiTriBufferSize = 1024 * 4;
    gl.bufferData(gl.ARRAY_BUFFER, this.uiTriBufferSize, gl.DYNAMIC_DRAW);

    this.screenQuadBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, this.screenQuadBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array([
      -1, -1,   -1, 1,   1, -1,   -1, 1,   1, 1,   1, -1
    ]), gl.STATIC_DRAW);

    const makeFramebuffer = (name, {size = fbSize, rb = false, filtering=gl.NEAREST} = {}) => {
      const texName = `${name}Tex`;
      const fb = this[name] = gl.createFramebuffer();
      gl.bindFramebuffer(gl.FRAMEBUFFER, fb);
      const tex = this[texName] = gl.createTexture();
      gl.bindTexture(gl.TEXTURE_2D, tex);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, filtering);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, filtering);
      gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, size.x, size.y, 0, gl.RGBA, gl.UNSIGNED_BYTE, null);
      gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, tex, 0);
      if (rb) {
        this[rb] = gl.createRenderbuffer();
        gl.bindRenderbuffer(gl.RENDERBUFFER, this[rb]);
        gl.renderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_COMPONENT16, fbSize.x, fbSize.y);
        gl.framebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.RENDERBUFFER, this[rb]);
        gl.bindRenderbuffer(gl.RENDERBUFFER, null);
      }
      return [fb, tex];
    };
    makeFramebuffer('fb', {rb: 'rb'});
    makeFramebuffer('postprocessA');
    makeFramebuffer('postprocessB');
    makeFramebuffer('offscreen', {size: V2(OFFSCREEN_SIZE, OFFSCREEN_SIZE)});

    gl.bindFramebuffer(gl.FRAMEBUFFER, null);

    //gl.disable(gl.DEPTH_TEST);
    //gl.depthFunc(gl.LEQUAL);
    gl.enable(gl.BLEND);
    gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
  }
  newFrame({ camera }) {
    const {gl} = this;
    const view = this.view = mat4.create();
    mat4.translate(view, view, [-camera.x, -camera.y, 0]);
    for (const program of Object.values(this.programs)) {
      if (program.uniforms.view) {
        gl.useProgram(program);
        this.setUniform(program.uniforms.view, view);
      }
    }
  }

  drawWorld_() {
    const {gl} = this;

    gl.clearColor(0.0, 0.0, 0.0, 0.0);
    if (this.tris) {
      let ti = 0;
      const verts = new Float32Array(this.tris * 7 * 3);
      const bufferQuadGroups = [];
      const sorted = [...this.gameQuadGroups].sort(([a], [b]) => a - b);
      for (const [zi, zGroup] of sorted) {
        for (const [shader, shaderGroup] of zGroup) {
          const shaderCmds = {shader, spans: []};
          for (const [texture, group] of shaderGroup) {
            const span = {texture, first: ti / 7, count: 0};
            shaderCmds.spans.push(span);
            for (const cmd of group) {
              const {vs, ex, size, z, alpha} = cmd;
              if (shader.program.uniforms.screenSampler) {
                const {screenTexBounds} = shader.uniforms;
                span.offscreen = size;
                const uv = span.offscreen.clone().divScalar(OFFSCREEN_SIZE);
                verts[ti++] = -1; verts[ti++] = -1; verts[ti++] = 0; verts[ti++] = ex[0].uvx; verts[ti++] = ex[0].uvy; verts[ti++] = ex[0].sx; verts[ti++] = ex[0].sy;
                verts[ti++] = -1; verts[ti++] =  1; verts[ti++] = 0; verts[ti++] = ex[1].uvx; verts[ti++] = ex[1].uvy; verts[ti++] = ex[1].sx; verts[ti++] = ex[1].sy;
                verts[ti++] =  1; verts[ti++] = -1; verts[ti++] = 0; verts[ti++] = ex[3].uvx; verts[ti++] = ex[3].uvy; verts[ti++] = ex[3].sx; verts[ti++] = ex[3].sy;
                verts[ti++] = -1; verts[ti++] =  1; verts[ti++] = 0; verts[ti++] = ex[1].uvx; verts[ti++] = ex[1].uvy; verts[ti++] = ex[1].sx; verts[ti++] = ex[1].sy;
                verts[ti++] =  1; verts[ti++] =  1; verts[ti++] = 0; verts[ti++] = ex[2].uvx; verts[ti++] = ex[2].uvy; verts[ti++] = ex[2].sx; verts[ti++] = ex[2].sy;
                verts[ti++] =  1; verts[ti++] = -1; verts[ti++] = 0; verts[ti++] = ex[3].uvx; verts[ti++] = ex[3].uvy; verts[ti++] = ex[3].sx; verts[ti++] = ex[3].sy;

                verts[ti++] = vs[0].x; verts[ti++] = vs[0].y; verts[ti++] = z; verts[ti++] = 0;    verts[ti++] = 0;    verts[ti++] = ex[0].l; verts[ti++] = alpha;
                verts[ti++] = vs[1].x; verts[ti++] = vs[1].y; verts[ti++] = z; verts[ti++] = 0;    verts[ti++] = uv.y; verts[ti++] = ex[1].l; verts[ti++] = alpha;
                verts[ti++] = vs[3].x; verts[ti++] = vs[3].y; verts[ti++] = z; verts[ti++] = uv.x; verts[ti++] = 0;    verts[ti++] = ex[3].l; verts[ti++] = alpha;
                verts[ti++] = vs[1].x; verts[ti++] = vs[1].y; verts[ti++] = z; verts[ti++] = 0;    verts[ti++] = uv.y; verts[ti++] = ex[1].l; verts[ti++] = alpha;
                verts[ti++] = vs[2].x; verts[ti++] = vs[2].y; verts[ti++] = z; verts[ti++] = uv.x; verts[ti++] = uv.y; verts[ti++] = ex[2].l; verts[ti++] = alpha;
                verts[ti++] = vs[3].x; verts[ti++] = vs[3].y; verts[ti++] = z; verts[ti++] = uv.x; verts[ti++] = 0;    verts[ti++] = ex[3].l; verts[ti++] = alpha;
              } else {
                verts[ti++] = vs[0].x; verts[ti++] = vs[0].y; verts[ti++] = z; verts[ti++] = ex[0].uvx; verts[ti++] = ex[0].uvy; verts[ti++] = ex[0].l; verts[ti++] = alpha;
                verts[ti++] = vs[1].x; verts[ti++] = vs[1].y; verts[ti++] = z; verts[ti++] = ex[1].uvx; verts[ti++] = ex[1].uvy; verts[ti++] = ex[1].l; verts[ti++] = alpha;
                verts[ti++] = vs[3].x; verts[ti++] = vs[3].y; verts[ti++] = z; verts[ti++] = ex[3].uvx; verts[ti++] = ex[3].uvy; verts[ti++] = ex[3].l; verts[ti++] = alpha;
                verts[ti++] = vs[1].x; verts[ti++] = vs[1].y; verts[ti++] = z; verts[ti++] = ex[1].uvx; verts[ti++] = ex[1].uvy; verts[ti++] = ex[1].l; verts[ti++] = alpha;
                verts[ti++] = vs[2].x; verts[ti++] = vs[2].y; verts[ti++] = z; verts[ti++] = ex[2].uvx; verts[ti++] = ex[2].uvy; verts[ti++] = ex[2].l; verts[ti++] = alpha;
                verts[ti++] = vs[3].x; verts[ti++] = vs[3].y; verts[ti++] = z; verts[ti++] = ex[3].uvx; verts[ti++] = ex[3].uvy; verts[ti++] = ex[3].l; verts[ti++] = alpha;
              }
              span.count += 6;
            }
          }
          bufferQuadGroups.push(shaderCmds);
        }
      }
      gl.bindBuffer(gl.ARRAY_BUFFER, this.triBuffer);
      const newBufferSize = verts.length * 4;
      if (newBufferSize > this.triBufferSize) {
        gl.bufferData(gl.ARRAY_BUFFER, verts, gl.DYNAMIC_DRAW);
        this.triBufferSize = newBufferSize;
      } else {
        gl.bufferSubData(gl.ARRAY_BUFFER, 0, verts);
      }
      gl.vertexAttribPointer(this.programs.world.attribs.position, 3, gl.FLOAT, false, 7*4, 0*4);
      gl.vertexAttribPointer(this.programs.world.attribs.extra, 4, gl.FLOAT, false, 7*4, 3*4);
      for (const {shader, spans} of bufferQuadGroups) {
        gl.useProgram(shader.program);
        for (const [key, value] of Object.entries(shader.uniforms)) {
          this.setUniform(shader.program.uniforms[key], value);
        }
        for (const {texture, first, count, offscreen} of spans) {
          if (offscreen) {
            gl.bindFramebuffer(gl.FRAMEBUFFER, this.offscreen);
            if (texture) {
              gl.activeTexture(gl.TEXTURE0);
              gl.bindTexture(gl.TEXTURE_2D, texture);
            }
            gl.activeTexture(gl.TEXTURE1);
            gl.bindTexture(gl.TEXTURE_2D, this.fbTex);
            gl.viewport(0, 0, offscreen.x, offscreen.y);
            gl.clear(gl.COLOR_BUFFER_BIT);
            gl.drawArrays(gl.TRIANGLES, first, count);
            gl.bindFramebuffer(gl.FRAMEBUFFER, this.fb);
            gl.useProgram(this.programs.world);
            gl.activeTexture(gl.TEXTURE0);
            gl.bindTexture(gl.TEXTURE_2D, this.offscreenTex);
            gl.viewport(0, 0, this.viewport.x, this.viewport.y);
            gl.drawArrays(gl.TRIANGLES, first + count, count);
            gl.useProgram(shader.program);
          } else {
            gl.bindTexture(gl.TEXTURE_2D, texture);
            gl.drawArrays(gl.TRIANGLES, first, count);
          }
        }
      }
    }
  }

  drawUI_() {
    const {gl} = this;
    let verts = new Float32Array(this.uiTris * 7 * 3);
    if (this.uiTris) {
      let ti = 0;
      for (const {a, b, color} of this.coloredQuads) {
        verts[ti++] = a.x; verts[ti++] = a.y; verts[ti++] = 0; verts[ti++] = color[0]; verts[ti++] = color[1]; verts[ti++] = color[2]; verts[ti++] = color[3];
        verts[ti++] = a.x; verts[ti++] = b.y; verts[ti++] = 0; verts[ti++] = color[0]; verts[ti++] = color[1]; verts[ti++] = color[2]; verts[ti++] = color[3];
        verts[ti++] = b.x; verts[ti++] = a.y; verts[ti++] = 0; verts[ti++] = color[0]; verts[ti++] = color[1]; verts[ti++] = color[2]; verts[ti++] = color[3];
        verts[ti++] = a.x; verts[ti++] = b.y; verts[ti++] = 0; verts[ti++] = color[0]; verts[ti++] = color[1]; verts[ti++] = color[2]; verts[ti++] = color[3];
        verts[ti++] = b.x; verts[ti++] = b.y; verts[ti++] = 0; verts[ti++] = color[0]; verts[ti++] = color[1]; verts[ti++] = color[2]; verts[ti++] = color[3];
        verts[ti++] = b.x; verts[ti++] = a.y; verts[ti++] = 0; verts[ti++] = color[0]; verts[ti++] = color[1]; verts[ti++] = color[2]; verts[ti++] = color[3];
      }
      const bufferQuadGroups = [];
      for (const [texture, group] of this.uiQuadGroups) {
        const span = {texture, first: ti / 7, count: 0};
        bufferQuadGroups.push(span);
        for (const cmd of group) {
          const {a, b, alpha, ex} = cmd;
          verts[ti++] = a.x; verts[ti++] = a.y; verts[ti++] = 0; verts[ti++] = ex[0].x; verts[ti++] = ex[0].y; verts[ti++] = ex[0].l; verts[ti++] = alpha;
          verts[ti++] = a.x; verts[ti++] = b.y; verts[ti++] = 0; verts[ti++] = ex[1].x; verts[ti++] = ex[1].y; verts[ti++] = ex[1].l; verts[ti++] = alpha;
          verts[ti++] = b.x; verts[ti++] = a.y; verts[ti++] = 0; verts[ti++] = ex[3].x; verts[ti++] = ex[3].y; verts[ti++] = ex[3].l; verts[ti++] = alpha;
          verts[ti++] = a.x; verts[ti++] = b.y; verts[ti++] = 0; verts[ti++] = ex[1].x; verts[ti++] = ex[1].y; verts[ti++] = ex[1].l; verts[ti++] = alpha;
          verts[ti++] = b.x; verts[ti++] = b.y; verts[ti++] = 0; verts[ti++] = ex[2].x; verts[ti++] = ex[2].y; verts[ti++] = ex[2].l; verts[ti++] = alpha;
          verts[ti++] = b.x; verts[ti++] = a.y; verts[ti++] = 0; verts[ti++] = ex[3].x; verts[ti++] = ex[3].y; verts[ti++] = ex[3].l; verts[ti++] = alpha;
          span.count += 6;
        }
      }
      gl.bindBuffer(gl.ARRAY_BUFFER, this.uiTriBuffer);
      const newBufferSize = verts.length * 4;
      if (newBufferSize > this.uiTriBufferSize) {
        gl.bufferData(gl.ARRAY_BUFFER, verts, gl.DYNAMIC_DRAW);
        this.uiTriBufferSize = newBufferSize;
      } else {
        gl.bufferSubData(gl.ARRAY_BUFFER, 0, verts);
      }
      gl.vertexAttribPointer(this.programs.color.attribs.position, 3, gl.FLOAT, false, 7*4, 0*4);
      gl.vertexAttribPointer(this.programs.color.attribs.extra, 4, gl.FLOAT, false, 7*4, 3*4);
      if (this.coloredQuads.length) {
        gl.useProgram(this.programs.color);
        gl.drawArrays(gl.TRIANGLES, 0, this.coloredQuads.length * 6);
      }
      if (bufferQuadGroups.length) {
        gl.useProgram(this.programs.ui);
        for (const {texture, first, count} of bufferQuadGroups) {
          gl.bindTexture(gl.TEXTURE_2D, texture);
          gl.drawArrays(gl.TRIANGLES, first, count);
        }
      }
    }
    if (this.lines) {
      gl.useProgram(this.programs.color);
      verts = new Float32Array(this.lines * 7 * 2);
      let li = 0;
      for (const {a, b, color} of this.boxes) {
        verts[li++] = a.x; verts[li++] = a.y; verts[li++] = 0; verts[li++] = color[0]; verts[li++] = color[1]; verts[li++] = color[2]; verts[li++] = color[3];
        verts[li++] = b.x; verts[li++] = a.y; verts[li++] = 0; verts[li++] = color[0]; verts[li++] = color[1]; verts[li++] = color[2]; verts[li++] = color[3];
        verts[li++] = b.x; verts[li++] = a.y; verts[li++] = 0; verts[li++] = color[0]; verts[li++] = color[1]; verts[li++] = color[2]; verts[li++] = color[3];
        verts[li++] = b.x; verts[li++] = b.y; verts[li++] = 0; verts[li++] = color[0]; verts[li++] = color[1]; verts[li++] = color[2]; verts[li++] = color[3];
        verts[li++] = b.x; verts[li++] = b.y; verts[li++] = 0; verts[li++] = color[0]; verts[li++] = color[1]; verts[li++] = color[2]; verts[li++] = color[3];
        verts[li++] = a.x; verts[li++] = b.y; verts[li++] = 0; verts[li++] = color[0]; verts[li++] = color[1]; verts[li++] = color[2]; verts[li++] = color[3];
        verts[li++] = a.x; verts[li++] = b.y; verts[li++] = 0; verts[li++] = color[0]; verts[li++] = color[1]; verts[li++] = color[2]; verts[li++] = color[3];
        verts[li++] = a.x; verts[li++] = a.y; verts[li++] = 0; verts[li++] = color[0]; verts[li++] = color[1]; verts[li++] = color[2]; verts[li++] = color[3];
      }
      gl.bindBuffer(gl.ARRAY_BUFFER, this.lineBuffer);
      const newBufferSize = verts.length * 4;
      if (newBufferSize > this.lineBufferSize) {
        gl.bufferData(gl.ARRAY_BUFFER, verts, gl.DYNAMIC_DRAW);
        this.lineBufferSize = newBufferSize;
      } else {
        gl.bufferSubData(gl.ARRAY_BUFFER, 0, verts);
      }
      gl.vertexAttribPointer(this.programs.color.attribs.position, 3, gl.FLOAT, false, 7*4, 0*4);
      gl.vertexAttribPointer(this.programs.color.attribs.extra, 4, gl.FLOAT, false, 7*4, 3*4);
      gl.drawArrays(gl.LINES, 0, this.lines * 2);
    }
  }
  endFrame({separation}) {
    const {gl} = this;
    const {canvas} = gl;

    gl.clearColor(0.0, 0.0, 0.0, 1.0);
    gl.bindFramebuffer(gl.FRAMEBUFFER, this.fb);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    this.drawWorld_();
    this.drawUI_();

    gl.bindBuffer(gl.ARRAY_BUFFER, this.screenQuadBuffer);
    gl.vertexAttribPointer(this.programs.screen.attribs.position, 2, gl.FLOAT, false, 2*4, 0);
    gl.disableVertexAttribArray(this.programs.world.attribs.extra);
    gl.clearColor(0.0, 0.0, 0.0, 1.0);

    if (this.config.postProcessing) {
      gl.useProgram(this.programs.highlights);
      gl.bindTexture(gl.TEXTURE_2D, this.fbTex);
      gl.bindFramebuffer(gl.FRAMEBUFFER, this.postprocessA);
      gl.drawArrays(gl.TRIANGLES, 0, 6);

      gl.useProgram(this.programs.blur);

      gl.bindFramebuffer(gl.FRAMEBUFFER, this.postprocessB);
      gl.bindTexture(gl.TEXTURE_2D, this.postprocessATex);
      this.setUniform(this.programs.blur.uniforms.blurVector, new Float32Array([1, 0]));
      gl.drawArrays(gl.TRIANGLES, 0, 6);

      gl.bindFramebuffer(gl.FRAMEBUFFER, this.postprocessA);
      gl.bindTexture(gl.TEXTURE_2D, this.postprocessBTex);
      this.setUniform(this.programs.blur.uniforms.blurVector, new Float32Array([0, 1]));
      gl.drawArrays(gl.TRIANGLES, 0, 6);
      gl.viewport(0, 0, this.viewport.x, this.viewport.y);
    } else {
      gl.bindFramebuffer(gl.FRAMEBUFFER, this.postprocessA);
      gl.clear(gl.COLOR_BUFFER_BIT);
    }

    gl.useProgram(this.programs.screen);
    if (this.config.postProcessing) {
      this.setUniform(this.programs.screen.uniforms.separation,
                      separation.map((v, i) => v / (i % 2 ? this.fbSize.y : this.fbSize.x)));
    } else {
      this.setUniform(this.programs.screen.uniforms.separation,
                      this.programs.screen.uniforms.separation.initial);
    }
    gl.bindFramebuffer(gl.FRAMEBUFFER, null);
    gl.clear(gl.COLOR_BUFFER_BIT);

    gl.bindTexture(gl.TEXTURE_2D, this.fbTex);
    gl.activeTexture(gl.TEXTURE1);
    gl.bindTexture(gl.TEXTURE_2D, this.postprocessATex);
    gl.activeTexture(gl.TEXTURE0);
    gl.drawArrays(gl.TRIANGLES, 0, 6);
    gl.enableVertexAttribArray(this.programs.world.attribs.extra);


    this.alpha = 1;
    this.z = 0;
    this.tris = 0;
    this.gameQuadGroups = new Map;
    this.uiQuadGroups = new Map;
    this.shaderArgs = new Map;
    this.coloredQuads = [];
    this.uiTris = 2;
    this.boxes = [];
    this.lines = 0;
  }
}
