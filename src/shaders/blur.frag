precision mediump float;

uniform sampler2D fb;
uniform vec2 pixelSize;
varying vec2 texCoord;

uniform float blurWeights[KSIZE];
uniform vec2 blurVector;

void main(void) {
  vec3 blur = texture2D(fb, texCoord).rgb * blurWeights[0];
  for (int i = 1; i < KSIZE; i++) {
    vec2 v = vec2(pixelSize * float(i) * blurVector);
    float w = blurWeights[i];
    blur += texture2D(fb, texCoord + v).rgb * w;
    blur += texture2D(fb, texCoord - v).rgb * w;
  }
  gl_FragColor = vec4(blur, 1.0);
}
