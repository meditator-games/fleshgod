precision mediump float;

varying vec4 color;

void main(void) {
  if (color.a == 0.0) discard;
  gl_FragColor = color;
}
