attribute vec2 position;

varying vec2 texCoord;
varying vec2 hi_texCoord;
uniform vec2 fbScale;
uniform vec2 hilightsScale;

void main(void) {
  gl_Position = vec4(position, 0.0, 1.0);
  texCoord = (position * 0.5 + 0.5) * fbScale;
  hi_texCoord = (position * 0.5 + 0.5) * hilightsScale;
}
