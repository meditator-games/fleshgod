precision mediump float;

uniform sampler2D fb;
varying vec2 texCoord;

void main(void) {
  vec3 color = texture2D(fb, texCoord).rgb;
  float brightness = dot(color, vec3(0.2126, 0.7152, 0.0722));
  if (brightness > 0.5) {
    gl_FragColor = vec4(color, 1.0);
  } else {
    gl_FragColor = vec4(0.0, 0.0, 0.0, 1.0);
  }
}
