attribute vec3 position;
attribute vec4 extra;

varying vec4 texCoord;

void main(void) {
  gl_Position = vec4(position, 1.0);
  texCoord = extra;
}
