precision mediump float;

uniform vec2 fbScale;
uniform float timer;

varying vec4 texCoord;

void main(void) {
  vec4 t1 = texture2D(prevScreen, texCoord.st);
  vec4 t2 = texture2D(nextScreen, texCoord.st);

  float uv = texCoord.st * fbScale;
  float d = (uv.y - uv.x * 0.5 - 1.0) * 4.0 + timer * 8.0;
  vec2 m = mod(uv, 0.1) / 0.1 * 2.0 - 1.0;
  d -= clamp(abs(pow(m.x, 16.0) + pow(m.y, 16.0)), 0.0, 1.0);

  gl_FragColor = mix(t1, t2, clamp(d, 0.0, 1.0));
}
