precision mediump float;

uniform sampler2D sampler;
uniform sampler2D screenSampler;
uniform vec4 screenTexBounds;
uniform float alpha;
varying vec4 texCoord;

void main(void) {
  vec4 texColor = texture2D(sampler, texCoord.st);
  vec2 midpoint = (screenTexBounds.xy + screenTexBounds.zw) / 2.0;
  vec2 uv = midpoint + (texCoord.pq - midpoint) / 1.5;
  vec4 screenColor = texture2D(screenSampler, uv);
  gl_FragColor = vec4(mix(screenColor.rgb, texColor.rgb, texColor.a * alpha), 1.0);
}
