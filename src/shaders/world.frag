precision mediump float;

uniform sampler2D sampler;
varying vec4 texCoord;

void main(void) {
  vec4 color = texture2D(sampler, texCoord.st);
  color.a *= texCoord.q;
  if (color.a == 0.0) discard;
  float light = texCoord.p;
  color = vec4(color.rgb * light, color.a);
  gl_FragColor = color;
}
