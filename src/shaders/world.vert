attribute vec3 position;
attribute vec4 extra;

uniform mat4 projection;
uniform mat4 view;
uniform vec2 fbScale;
varying vec4 texCoord;
varying vec2 fbPosition;

void main(void) {
  gl_Position = projection * view * vec4(floor(position + 0.5), 1.0);
  fbPosition = (gl_Position.xy * 0.5 + 0.5) * fbScale;
  texCoord = extra;
}
