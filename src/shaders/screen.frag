precision mediump float;

uniform sampler2D fb;
uniform sampler2D hilights;
uniform vec2 separation[3];
varying vec2 texCoord;
varying vec2 hi_texCoord;

vec3 sep(in sampler2D tex, in vec2 uv) {
  float red = texture2D(tex, uv + separation[0]).r;
  float green = texture2D(tex, uv + separation[1]).g;
  float blue = texture2D(tex, uv + separation[2]).b;
  return vec3(red, green, blue);
}

void main(void) {
  vec3 color = sep(fb, texCoord);
  vec3 bloom = sep(hilights, hi_texCoord);
  gl_FragColor = vec4(color + bloom * 0.5, 1.0);
}
