precision mediump float;

uniform sampler2D sampler;
varying vec4 texCoord;

void main(void) {
  vec4 color = texture2D(sampler, texCoord.st);
  color.a *= texCoord.q;
  if (color.a == 0.0) discard;
  gl_FragColor = color;
}
