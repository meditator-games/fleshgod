precision mediump float;

uniform vec2 fbSize;
uniform sampler2D sampler;
uniform vec4 screenTexBounds;
uniform float phase;
varying vec4 texCoord;
varying vec2 fbPosition;

const float PI = 3.141592653589793;

void main(void) {
  float ystep = (fbPosition.y - screenTexBounds.y) /
    (screenTexBounds.w - screenTexBounds.y) + phase;
  vec2 uv = texCoord.st + vec2(sin(ystep * PI * 2.0) * 6.0/fbSize.x, 0.0);
  gl_FragColor = texture2D(sampler, uv);
}
