attribute vec3 position;
attribute vec4 extra;

uniform mat4 projection;

varying vec4 color;

void main(void) {
  gl_Position = projection * vec4(position, 1.0);
  color = extra;
}
