precision mediump float;

uniform sampler2D screenSampler;
uniform vec4 screenTexBounds;
uniform float thickness;
uniform float depth;
uniform float curve;
uniform mat3 transform;

varying vec4 texCoord;

void main(void) {
  vec2 uv = texCoord.pq;
  float borderWidth = thickness / 4.0;
  float radius = 0.5 - borderWidth;
  vec2 qsize = screenTexBounds.zw - screenTexBounds.xy;
  vec2 midpoint = (screenTexBounds.xy + screenTexBounds.zw) / 2.0;
  vec2 rv = (transform * vec3((uv - midpoint) / qsize, 1.0)).xy;
  float d = abs(length(rv) - radius);
  if (d < borderWidth) {
    float cr = d / borderWidth;
    vec2 dpoint = normalize(rv) * radius;
    uv += (rv - dpoint) * sqrt(1.0 - pow(cr, 4.0)) * depth;
  }
  gl_FragColor = texture2D(screenSampler, uv);
}
