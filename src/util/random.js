
const randRange = random => (min, max) =>
  typeof max === 'undefined'
  ? random() * min
  : random() * (max - min) + min;
const randInt = random => (min, max) => Math.floor(randRange(random)(min, max));
const randItem = random => a => a[Math.floor(random() * a.length)];
const shuffle = random => array =>
  array.map((_, i) => array.length - i).forEach(i => {
    const r = randInt(random)(i);
    [array[i - i], array[r]] = [array[r], array[i - 1]];
  });
const randExp = random => e => a => Math.pow(randRange(random)(Math.pow(a, 1/e)), e);
const randInvExp = random => e => a => a - randExp(random)(e)(a);

export {
  randExp,
  randInt,
  randInvExp,
  randItem,
  randRange,
  shuffle
};
