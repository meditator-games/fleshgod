
import AR2 from './arect2.js';
import Circ from './circle.js';
import V2 from './vector2.js';

export default Seg2;

class LineSeg2 {
  constructor(ax, ay, bx, by) {
    this.ax = ax;
    this.ay = ay;
    this.bx = bx;
    this.by = by;
  }
  get a() {
    return V2(this.ax, this.ay);
  }
  get b() {
    return V2(this.bx, this.by);
  }
  get vector() {
    return this.b.subV2(this.a);
  }
  get aabb() {
    return AR2.fromPoints(
      V2(Math.min(this.ax, this.bx), Math.min(this.ay, this.by)),
      V2(Math.max(this.ax, this.bx), Math.max(this.ay, this.by)));
  }
  get midpoint() {
    return this.a.addV2(this.vector.mulScalar(0.5));
  }
  clone() {
    return new LineSeg2(this.ax, this.ay, this.bx, this.by);
  }
  translate({x, y}) {
    this.ax += x;
    this.ay += y;
    this.bx += x;
    this.by += y;
    return this;
  }
  scale({x, y}) {
    this.ax *= x;
    this.ay *= y;
    this.bx *= x;
    this.by *= y;
    return this;
  }
  rotate(t) {
    const a = this.a.rotate(t);
    const b = this.b.rotate(t);
    this.ax = a.x;
    this.ay = a.y;
    this.bx = b.x;
    this.by = b.y;
    return this;
  }
  intersects(other) {
    if (other instanceof LineSeg2) {
      const av = this.vector;
      const t = av.angle;
      const ro = other.clone().translate(this.a.mulScalar(-1)).rotate(-t);
      const ody = ro.by - ro.ay;
      const b = ody === 0 ? ro.ax : ro.ax - ro.ay * ((ro.bx - ro.ax) / ody);
      const ys = Math.sign(ro.ay);
      if ((ys !== Math.sign(ro.by) || ys === 0) && b >= 0 && b <= av.length()) {
        return V2(b, 0).rotate(t).addV2(this.a);
      }
      return false;
    }
    if (other instanceof Circ.class) {
      const av = this.b.subV2(this.a);
      const t = av.angle;
      const rc = other.center.subV2(this.a).rotate(-t);
      const yd = Math.abs(rc.y);
      if (other.r < yd) {
        return false;
      }
      const ix = Math.sqrt(other.r * other.r - yd * yd);
      return rc.x + ix > 0
        && av.length() > rc.x - ix;
    }
    if (other instanceof AR2.class) {
      return other.contains(this.a)
        || other.contains(this.b)
        || Seg2.fromPoints(other.topLeft, other.topRight).intersects(this)
        || Seg2.fromPoints(other.topRight, other.bottomRight).intersects(this)
        || Seg2.fromPoints(other.bottomRight, other.bottomLeft).intersects(this)
        || Seg2.fromPoints(other.bottomLeft, other.topLeft).intersects(this);
    }
    return other.intersects(this);
  }
}

Seg2.fromPoints = (a, b) => Seg2(a.x, a.y, b.x, b.y);
Seg2.class = LineSeg2;

function Seg2(ax, ay, bx, by) {
  return new LineSeg2(ax, ay, bx, by);
}
