
import AR2 from './arect2.js';
import Circ from './circle.js';
import Seg2 from './lineseg2.js';
import V2 from './vector2.js';

export default Poly2;

class ConvexPolygon2 {
  constructor(points) {
    this.points = points;
  }
  get lines() {
    return this.points.map((p, i) =>
      Seg2.fromPoints(p, this.points[(i + 1) % this.points.length]));
  }
  clone() {
    return new ConvexPolygon2(this.points.map(p => p.clone()));
  }
  translate({x, y}) {
    for (const p of this.points) {
      p.x += x;
      p.y += y;
    }
  }
  rotate(t) {
    for (const p of this.points) {
      p.rotate(t);
    }
  }
  scale({x, y}) {
    for (const p of this.points) {
      p.x *= x;
      p.y *= y;
    }
  }
  get aabb() {
    if (!this.points.length) return null;
    const min = this.points[0].clone();
    const max = this.points[0].clone();
    for (const p of this.points.slice(1)) {
      min.x = Math.min(min.x, p.x);
      min.y = Math.min(min.y, p.y);
      max.x = Math.max(max.x, p.x);
      max.y = Math.max(max.y, p.y);
    }
    return AR2.fromPoints(min, max);
  }
  get midpoint() {
    const sum = V2();
    for (const p of this.points) {
      sum.addV2(p);
    }
    return sum.divScalar(this.points.length);
  }
  contains({x, y}) {
    const {points: ps} = this;
    let result = false;
    let j = ps.length - 1;
    for (let i = 0; i < ps.length; i += 1) {
      const a = ps[j];
      const b = ps[i];
      if (((b.y > y) !== (a.y > y))
        && (x < b.x + (a.x - b.x) * (y - b.y) / (a.y - b.y))) {
        result = !result;
      }
      j = i;
    }
    return result;
  }
  intersects(other) {
    if (!this.points.length) return false;
    const axes = p => {
      const as = [];
      let j = p.points.length - 1;
      for (let i = 0; i < p.points.length; i += 1) {
        const curr = p.points[j];
        const next = p.points[i];
        const n = curr.clone().subV2(next).rotate90();
        as.push(n);
        j = i;
      }
      return as;
    };
    const project = (p, axis) => {
      let dp = p.points[0].dot(axis);
      const bounds = [dp, dp];
      for (let i = 1; i < p.points.length; i += 1) {
        dp = p.points[i].dot(axis);
        bounds[0] = Math.min(dp, bounds[0]);
        bounds[1] = Math.max(dp, bounds[1]);
      }
      return bounds;
    };
    const overlap = (a, b, axis) => {
      const pa = project(a, axis);
      const pb = project(b, axis);
      return pa[0] < pb[1] && pa[1] > pb[0];
    };
    if (other instanceof ConvexPolygon2) {
      for (const axis of [...axes(this), ...axes(other)]) {
        if (!overlap(this, other, axis)) {
          return false;
        }
      }
      return true;
    }
    if (other instanceof Seg2.class) {
      const lp = Poly2(other.a, other.b);
      if (!overlap(this, lp, other.vector.rotate90())) {
        return false;
      }
      for (const axis of [...axes(this)]) {
        if (!overlap(this, lp, axis)) {
          return false;
        }
      }
      return true;
    }
    if (other instanceof Circ.class) {
      let j = this.points.length - 1;
      const ps = [];
      let circleInside = true;
      for (let i = 0; i < this.points.length; i += 1) {
        const curr = this.points[j];
        const next = this.points[i];
        const axis = next.clone().subV2(curr);
        const dp = other.center.subV2(curr).dot(axis);
        const l = axis.lengthSquared;
        const proj = V2(dp / l * axis.x, dp / l * axis.y);
        if (dp >= 0 && dp <= l) {
          ps.push(proj.clone().addV2(curr));
        } else {
          ps.push(curr.clone());
        }
        if (circleInside) {
          const naa = axis.clone().normalize().mulScalar(other.r);
          const nab = naa.clone().reverse();
          const dpa = other.center.addV2(naa).subV2(curr).dot(axis);
          const dpb = other.center.addV2(nab).subV2(curr).dot(axis);
          if (!(dpa < l && dpb > 0)) {
            circleInside = false;
          }
        }
        j = i;
      }
      if (circleInside) {
        return true;
      }
      for (const p of ps) {
        if (p.subV2(other.center).length() < other.r) {
          return true;
        }
      }
      return false;
    }
    if (other instanceof AR2.class) {
      return this.intersects(other.poly);
    }
    return false;
  }
}

function Poly2(...points) {
  return new ConvexPolygon2(points);
}

Poly2.class = ConvexPolygon2;
