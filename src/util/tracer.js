
export default function tracer(
  {x: xa, y: ya}, {x: xb, y: yb},
  {cellSize: cs, callback = p => p}) {

  const dx = xb - xa;
  const dy = yb - ya;
  const adx = Math.abs(dx);
  const ady = Math.abs(dy);
  const sx = Math.sign(dx);
  const sy = Math.sign(dy);
  const cxa = Math.floor(xa / cs) * cs;      // cell positions
  const cxb = Math.floor(xb / cs) * cs;
  const cya = Math.floor(ya / cs) * cs;
  const cyb = Math.floor(yb / cs) * cs;
  const cells = [];
  if (adx > ady) {
    const acsy = cs * (ady / adx);                   // step in y corresponding to cs step in x
    const cxo = (dx > 0 ? xa - cxa : cs - (xa - cxa)); // projected start x offset in first cell
    const ys = ya - cxo * (ady / adx) * sy;          // projected start point in global coords
    const cyo = Math.floor(ys / cs) * cs;            // projected first cell
    const ayp = dy > 0 ? ys - cyo : (cyo + cs) - ys; // projected start y offset in first cell
    let ey = Math.floor((ayp + acsy) / cs) * cs;
    { // first column
      for (let cy = Math.abs(cya - cyo); cy <= ey; cy += cs) {
        const p = V2(cxa, cyo + cy * sy);
        const r = callback(p);
        if (!r) return cells;
        cells.push(r);
      }
    }
    let ix = 1;
    const xe = adx + cxo - cs;
    for (let cx = cs; cx < xe; cx += cs, ix += 1) {
      const x = cxa + cx * sx;
      const ney = Math.floor((ayp + acsy * (ix + 1)) / cs) * cs;
      for (let cy = ey; cy <= ney; cy += cs) {
        const p = V2(x, cyo + cy * sy);
        const r = callback(p);
        if (!r) return cells;
        cells.push(r);
      }
      ey = ney;
    }
    { // last column
      const cx = cxa + ix * cs * sx;
      const ney = Math.abs(cyb - cyo);
      for (let cy = ey; cy <= ney; cy += cs) {
        const p = V2(cx, cyo + cy * sy);
        const r = callback(p);
        if (!r) return cells;
        cells.push(r);
      }
    }
  } else { // same algorithm but with x/y swapped
    const acsx = cs * (adx / ady);
    const cyo = (dy > 0 ? ya - cya : cs - (ya - cya));
    const xs = xa - cyo * (adx / ady) * sx;
    const cxo = Math.floor(xs / cs) * cs;
    const axp = dx > 0 ? xs - cxo : (cxo + cs) - xs;
    let ex = Math.floor((axp + acsx) / cs) * cs;
    { // first row
      for (let cx = Math.abs(cxa - cxo); cx <= ex; cx += cs) {
        const p = V2(cxo + cx * sx, cya);
        const r = callback(p);
        if (!r) return cells;
        cells.push(r);
      }
    }
    let iy = 1;
    const ye = ady + cyo - cs;
    for (let cy = cs; cy < ye; cy += cs, iy += 1) {
      const y = cya + cy * sy;
      const nex = Math.floor((axp + acsx * (iy + 1)) / cs) * cs;
      for (let cx = ex; cx <= nex; cx += cs) {
        const p = V2(cxo + cx * sx, y);
        const r = callback(p);
        if (!r) return cells;
        cells.push(r);
      }
      ex = nex;
    }
    { // last row
      const cy = cya + iy * cs * sy;
      const nex = Math.abs(cxb - cxo);
      for (let cx = ex; cx <= nex; cx += cs) {
        const p = V2(cxo + cx * sx, cy);
        const r = callback(p);
        if (!r) return cells;
        cells.push(r);
      }
    }
  }
  return cells;
}
