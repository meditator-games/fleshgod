import AR2 from './arect2.js';
import V2 from './vector2.js';

export default Circ;

class Circle {
  constructor(x, y, r) {
    this.x = x;
    this.y = y;
    this.r = r;
  }
  clone() {
    return Circle(this.x, this.y, this.r);
  }
  translate({x, y}) {
    this.ax += x;
    this.ay += y;
    return this;
  }
  contains(point) {
    return this.center.subV2(point).length() < this.r;
  }
  intersects(other) {
    if (other instanceof AR2.class) {
      const n = V2(Math.max(other.x, Math.min(this.x, other.x + other.w)),
                   Math.max(other.y, Math.min(this.y, other.y + other.h)));
      return this.contains(n);
    }
    if (other instanceof Circle) {
      return other.center.subV2(this.center).length() < other.r + this.r;
    }
    return other.intersects(this);
  }
  get center() {
    return V2(this.x, this.y);
  }
  get aabb() {
    return AR2(this.x - this.r, this.y - this.r, this.r * 2, this.r * 2);
  }
}

function Circ(x, y, r) {
  return new Circle(x, y, r);
}

Circ.class = Circle;
