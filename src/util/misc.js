
export { bindAll, clone, deepFreeze, remove, removeAll, partition, upperBound };

function bindAll(obj) {
  let proto = Object.getPrototypeOf(obj);
  while (proto.constructor !== Object) {
    const ds = Object.entries(Object.getOwnPropertyDescriptors(proto));
    for (const [name, {value}] of ds) {
      if (name !== 'constructor' && typeof value === 'function') {
        obj[name] = value.bind(obj);
      }
    }
    proto = Object.getPrototypeOf(proto);
  }
}

function clone(obj) {
  if (Array.isArray(obj)) {
    return obj.slice().map(x => clone(x));
  }
  if (obj) {
    const proto = Object.getPrototypeOf(obj);
    if (proto.constructor === Map || proto.constructor === WeakMap) {
      return new proto.constructor(clone([...obj.entries()]));
    }
    if (proto.constructor === Set || proto.constructor === WeakSet) {
      return new proto.constructor(clone([...obj]));
    }
    if (typeof obj === 'object') {
      return Object.keys(obj).reduce((o, k) => (o[k] = clone(obj[k]), o), {});
    }
  }
  return obj;
}

function remove(array, item) {
  const index = array.indexOf(item);
  if (index !== -1) {
    array.splice(index, 1);
  }
}

function removeAll(array, item) {
  let index = array.indexOf(item);
  while (index !== -1) {
    array.splice(index, 1);
    index = array.indexOf(item);
  }
}

function partition(array, predicate) {
  return array.reduce(([f, t], v, ...a) => predicate(v, ...a)
      ? [f, [...t, v]] : [[...f, v], t], [[], []]);
}

function deepFreeze(obj) {
  if (Array.isArray(obj)) {
    for (const v of obj) deepFreeze(v);
  } else if (obj && typeof obj === 'object') {
    for (const k of Object.keys(obj)) deepFreeze(obj[k]);
  }
  return Object.freeze(obj);
}

function upperBound(value, array, comp = (a, b) => a < b) {
  let first = 0;
  let index = 0;
  let count = array.length;
  let step = 0;

  while (count > 0) {
    index = first;
    step = count >> 1;
    index += step;
    if (!comp(value, array[index])) {
      first = ++index;
      count -= step + 1;
    } else {
      count = step;
    }
  }
  return first;
}
