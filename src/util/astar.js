export default astar;
import PriorityQueue from './priority-queue.js';

function astar(start, goal, { neighbors, passable, heuristic, cost }) { // eslint-disable-line no-unused-vars
  const priorities = new Map();
  const open = new PriorityQueue([], (a, b) => {
    const ap = priorities.get(a);
    const bp = priorities.get(b);
    if (ap > bp) return 1;
    if (ap < bp) return -1;
    return 0;
  });
  const cameFrom = new Map();
  const costs = new Map();
  let current = null;
  let totalCost = null;

  costs.set(start, 0);
  priorities.set(start, 0);
  open.enqueue(start);

  passable = passable || (() => true);
  heuristic = heuristic || (() => 0);
  cost = cost || (() => 1);

  while (open.length) {
    current = open.dequeue();
    if (current === goal) {
      return buildPath(cameFrom, current);
    }

    totalCost = costs.get(current);
    neighbors(current).map(visitNeighbor);
  }
  return [];

  function visitNeighbor(neighbor) {
    const neighborCost = totalCost + cost(current, neighbor);
    if (passable(neighbor) && (!costs.has(neighbor) || neighborCost < costs.get(neighbor))) {
      const priority = neighborCost + heuristic(goal, neighbor);

      costs.set(neighbor, neighborCost);
      priorities.set(neighbor, priority);
      open.enqueue(neighbor);
      cameFrom.set(neighbor, current);
    }
  }

  function buildPath(cameFrom, target) {
    if (!target) {
      return [];
    }
    const from = cameFrom.get(target);
    const path = buildPath(cameFrom, from);
    return [...path, target];
  }
}


