
export default V2;

class Vector2 {
  constructor(x, y) {
    this.x = x || 0;
    this.y = (typeof y === 'undefined' ? x : y) || 0;
  }
  clone() {
    return V2(this.x, this.y);
  }
  setX(x) {
    this.x = x;
    return this;
  }
  setY(y) {
    this.y = y;
    return this;
  }
  map(f) {
    return V2(f(this.x), f(this.y));
  }
  transform(f) {
    this.x = f(this.x);
    this.y = f(this.y);
    return this;
  }
  set(x, y) {
    this.x = x;
    this.y = y;
    return this;
  }
  add(x, y) {
    this.x += x;
    this.y += y;
    return this;
  }
  addScalar(v) {
    this.x += v;
    this.y += v;
    return this;
  }
  addV2({x, y}) {
    this.x += x;
    this.y += y;
    return this;
  }
  sub(x, y) {
    this.x -= x;
    this.y -= y;
    return this;
  }
  subScalar(v) {
    this.x -= v;
    this.y -= v;
    return this;
  }
  subV2({x, y}) {
    this.x -= x;
    this.y -= y;
    return this;
  }
  mul(x, y) {
    this.x *= x;
    this.y *= y;
    return this;
  }
  mulScalar(v) {
    this.x *= v;
    this.y *= v;
    return this;
  }
  mulV2({x, y}) {
    this.x *= x;
    this.y *= y;
    return this;
  }
  div(x, y) {
    this.x /= x;
    this.y /= y;
    return this;
  }
  divScalar(v) {
    this.x /= v;
    this.y /= v;
    return this;
  }
  divV2({x, y}) {
    this.x /= x;
    this.y /= y;
    return this;
  }
  mod(x, y) {
    this.x %= x;
    this.y %= y;
    return this;
  }
  modScalar(v) {
    this.x %= v;
    this.y %= v;
    return this;
  }
  modV2({x, y}) {
    this.x %= x;
    this.y %= y;
    return this;
  }
  rotate(t) {
    const st = Math.sin(t);
    const ct = Math.cos(t);
    const {x, y} = this;
    this.x = ct * x - st * y;
    this.y = ct * y + st * x;
    return this;
  }
  dot(other) {
    return this.x * other.x + this.y * other.y;
  }
  cross(other) {
    return this.x * other.y - this.y * other.x;
  }
  rotate90() {
    [this.x, this.y] = [this.y, -this.x];
    return this;
  }
  reverse() {
    [this.x, this.y] = [-this.x, -this.y];
    return this;
  }
  get angle() {
    return Math.atan2(this.y, this.x);
  }
  get empty() {
    return Boolean(this.x || this.y);
  }
  length() {
    return Math.sqrt(this.x * this.x + this.y * this.y);
  }
  get lengthSquared() {
    return this.x * this.x + this.y * this.y;
  }
  manhattanLength() {
    return Math.abs(this.x) + Math.abs(this.y);
  }
  normalize() {
    const len = this.length();
    if (!len) return this.set(0, 0);
    return this.divScalar(len);
  }
  equals(other) {
    return this.x === other.x && this.y === other.y;
  }
  array() {
    return [this.x, this.y];
  }
  toString() {
    return `[${this.x}, ${this.y}]`;
  }
  toStyle(sx='left', sy='top') {
    return {[sx]: `${this.x}px`, [sy]: `${this.y}px`};
  }
  toJSON() {
    return [this.x, this.y];
  }
}

function V2(x, y) {
  return new Vector2(x, y);
}

V2.class = Vector2;
