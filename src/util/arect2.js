
import Poly2 from './polygon2.js';
import V2 from './vector2.js';

export default AR2;

AR2.fromPoints = (a, b) => AR2(a.x, a.y, b.x - a.x, b.y - a.y);
AR2.fromPointAndSize = (p, s) => AR2(p.x, p.y, s.x, s.y);

class ARect2 {
  constructor(x, y, w, h) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
  }
  translate({x, y}) {
    this.x += x;
    this.y += y;
    return this;
  }
  scale({x, y}) {
    this.x *= x;
    this.y *= y;
    this.w *= x;
    this.h *= y;
    return this;
  }
  contains(point) {
    return point.x >= this.x
      && point.x < this.x + this.w
      && point.y >= this.y
      && point.y < this.y + this.h;
  }
  intersects(other) {
    if (other instanceof ARect2) {
      return this.x < other.x + other.w
        && this.x + this.w > other.x
        && this.y < other.y + other.h
        && this.y + this.h > other.y;
    }
    return other.intersects(this);
  }
  get topLeft() {
    return V2(this.x, this.y);
  }
  get topRight() {
    return V2(this.x + this.w, this.y);
  }
  get bottomLeft() {
    return V2(this.x, this.y + this.h);
  }
  get bottomRight() {
    return V2(this.x + this.w, this.y + this.h);
  }
  get midpoint() {
    const tl = this.topLeft;
    return this.bottomRight.subV2(tl).mulScalar(0.5).addV2(tl);
  }
  get size() {
    return V2(this.w, this.h);
  }
  get poly() {
    return Poly2(
      this.topLeft,
      this.topRight,
      this.bottomRight,
      this.bottomLeft);
  }
  rotated(t) {
    return Poly2(
      this.topLeft.rotate(t),
      this.topRight.rotate(t),
      this.bottomRight.rotate(t),
      this.bottomLeft.rotate(t));
  }
  clone() {
    return AR2(this.x, this.y, this.w, this.h);
  }
}

function AR2(x, y, w, h) {
  return new ARect2(x, y, w, h);
}

AR2.class = ARect2;
