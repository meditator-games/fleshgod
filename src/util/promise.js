export { allKeys };

function allKeys(obj) {
  const keys = Object.keys(obj);
  return Promise.all(keys.map(k => Promise.resolve(obj[k])))
    .then(results => results.reduce((o, r, i) => (o[keys[i]] = r, o), {}));
}
