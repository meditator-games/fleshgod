import * as Sound from './sound.js';
import localForage from 'localforage';
import { DEFAULT_FONT_SIZE } from './defs.js';
import { allKeys } from './util/promise.js';
import { error } from './debug.js';
import { VDOM } from './framework.js';
import UI from './ui.jsx';
import ATLASES from 'atlases';
import untar from 'js-untar';

window.addEventListener('DOMContentLoaded', () => bootstrap(window));

const defaultConfig = () => ({
  sfxVolume: 0.3,
  musicVolume: 0.2,
  gl: true,
  postProcessing: true
});

const FONTS = Object.freeze(['Roboto', 'ArcadePix']);

async function bootstrap(window) { // eslint-disable-line no-unused-vars
  window.addEventListener('error', e => (error(e), e.preventDefault()));
  window.addEventListener('unhandledrejection', e => (error(e.reason), e.preventDefault()));
  const canvas = window.document.getElementById('canvas');
  const loading = document.getElementById('loading');
  const spinner = loading.querySelector('.spinner');
  const spinnerVal = loading.querySelector('.progress');

  const AudioContext = window.AudioContext || window.webkitAudioContext;
  const audio = AudioContext ? new AudioContext() : null;
  const progress = {main: 0, gfx:0, snd:0, fonts: 0};
  const updateProgress = () => {
    const vals = Object.values(progress);
    const frac = vals.reduce((sum, p) => sum + p, 0)/vals.length;
    spinnerVal.innerText = `${Math.round(frac*100)}%`;
    spinner.style.animationDuration = `${10 - frac * 9.5}s`;
  };

  const loadTar = async (file, module) =>
    untar(await new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest;
      xhr.responseType = 'arraybuffer';
      xhr.addEventListener('progress', ({total, loaded}) =>
        total > 0 && (progress[module] = loaded/total, updateProgress()));
      xhr.addEventListener('load', () => resolve(xhr.response));
      xhr.addEventListener('error', e => reject(e));
      xhr.addEventListener('abort', e => reject(e));
      xhr.open('GET', file, true);
      xhr.send();
    }));
  const loadImage = filename => new Promise((resolve, reject) => {
    const img = new Image;
    img.addEventListener('load', () => resolve(img));
    img.addEventListener('error', e => reject(`Error loading ${filename}`));
    img.src = filename;
  });
  const loadFont = font =>
    window.document.fonts.load(`${DEFAULT_FONT_SIZE}px ${font}`)
      .then(f => (progress.fonts += 1/FONTS.length, updateProgress(), font));
  const loadFonts = names => allKeys(names.reduce((o, n) =>
        (o[n] = loadFont(n), o), {}));

  loading.style.display = '';

  const vdom = new VDOM(window.document);
  const ui = new UI(window, vdom);
  if (ui.webglSupported) {
    progress.gl = 0;
  }

  const moduleLoaded = name => ({default: def}) => {
    progress[name] = 1;
    updateProgress();
    return def;
  };

  try {
    const runtime = await allKeys({
      GLBackend: ui.webglSupported &&
        import(/* webpackChunkName: 'webgl' */ './webgl-backend.js')
        .then(moduleLoaded('gl')),
      Main:
        import(/* webpackPrefetch: true, webpackChunkName: 'game' */ './main.js')
        .then(moduleLoaded('main')),
      gfx: loadTar('./gfx.tar', 'gfx')
        .then(entries => Promise.all(entries.map(async ({blob, name}) => {
          const image = await loadImage(URL.createObjectURL(blob));
          const id = parseInt(name, 10);
          return Object.assign({image}, ATLASES[id]);
        }))),
      sounds: loadTar('./snd.tar', 'snd')
        .then(entries => allKeys(entries.reduce((sounds, {buffer, name}) => {
          const id = name.replace(/\.mp3$/,'');
          sounds[id] = Sound.load(audio, buffer);
          return sounds;
        }, {}))),
      fonts: loadFonts(FONTS),
      config: localForage.getItem('fleshgodSettings')
        .then(config => Object.assign(defaultConfig(), JSON.parse(config)))
        .catch(() => defaultConfig()),
      canvas,
      audio,
      ui,
      window
    });

    canvas.style.display = '';
    loading.style.display = 'none';
    vdom.render(ui.render(runtime.config), window.document.body);
    runtime.Main(runtime);
  } catch (e) {
    error(e);
  }
}
