
export { error, debug, log };

const LOG_SERVER = 'http://192.168.0.8:3000'

function printError(msg) {
  if (msg instanceof ErrorEvent) {
    msg = msg.error;
  }
  if (msg instanceof Error) {
    msg = `${msg.message}\n${msg.stack}`;
  }
  if (typeof msg !== 'string') {
    msg = JSON.stringify(msg);
  }
  return msg;
}

function log(msg) {
  return fetch(`${LOG_SERVER}/api/v1/error`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({ message: printError(msg) }),
    mode: 'cors'
  }).catch(() => 0);
}

function error(msg) {
  if (msg instanceof ErrorEvent) {
    console.error(msg.error);
  } else {
    console.error(msg);
  }
  if (!PRODUCTION) {
    log(msg);
  }
  window.alert(printError(msg));
}

function debug(...args) {
  if (!PRODUCTION) {
    const elem = window.document.getElementById('debug');
    elem.value = args.join(' ');
  }
}

