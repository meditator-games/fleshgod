import {CELL_MASK, CELL_SHIFT, CELL_SIZE, SPRITES} from './defs.js';
import AR2 from './util/arect2.js';
import Circ from './util/circle.js';
import Poly2 from './util/polygon2.js';
import V2 from './util/vector2.js';
import {hitbox} from './physics.js';
import {remove} from './util/misc.js';

const DIRS = Object.freeze(['left', 'right', 'up', 'down']);

export default class Board {
  constructor() {
    this.clear();
  }
  clear() {
    this.cells = new Map();
    this.sprites = new Map(Object.keys(SPRITES).map(t => [t, []]));
    this.enemies = [];
    this.inactive = [];
    this.pickups = [];
  }
  toJSON(extra) {
    const json = { sprites: [], extra };
    const types = new Set();
    const pushSprite = s => {
      const sprite = [s.initial.pos.x, s.initial.pos.y, s.type];
      types.add(s.type);
      for (const prop of SPRITES[s.type].props || []) {
        if (prop === 'dir') {
          sprite.push(DIRS.indexOf(s.initial[prop]));
        }
      }
      json.sprites.push(sprite);
    };
    for (const sprite of this.enemies) {
      pushSprite(sprite);
    }
    for (const sprite of this.pickups) {
      pushSprite(sprite);
    }
    for (const [type, group] of this.sprites) {
      if (!SPRITES[type].particle && !SPRITES[type].enemy && !SPRITES[type].pickup) {
        for (const sprite of group) {
          pushSprite(sprite);
        }
      }
    }
    json.types = [...types];
    for (const sprite of json.sprites) {
      sprite[2] = json.types.indexOf(sprite[2]);
    }
    return json;
  }
  fromJSON(json) {
    this.clear();
    for (const sprite of json.sprites) {
      const result = this.createSprite(V2(sprite[0], sprite[1]), json.types[sprite[2]]);
      let index = 3;
      for (const prop of SPRITES[result.type].props || []) {
        if (prop === 'dir') {
          result[prop] = DIRS[sprite[index]];
          result.initial[prop] = result[prop];
          index += 1;
        }
      }
    }
  }
  cell({x, y}) {
    return this.cells.get(`${x >> CELL_SHIFT},${y >> CELL_SHIFT}`);
  }
  deleteCell({x, y}) {
    this.cells.delete(`${x >> CELL_SHIFT},${y >> CELL_SHIFT}`);
  }
  cellCreate({x, y}) {
    const key = `${x >> CELL_SHIFT},${y >> CELL_SHIFT}`;
    let cell = this.cells.get(key);
    if (cell) return cell;
    cell = { sprites: [], pos: V2(x, y) };
    this.cells.set(key, cell);
    return cell;
  }
  static shapeEach(s, func) {
    if (s instanceof Circ.class) {
      for (let y = (s.y - s.r) & CELL_MASK; y < s.y + s.r; y += CELL_SIZE) {
        for (let x = (s.x - s.r) & CELL_MASK; x < s.x + s.r; x += CELL_SIZE) {
          func(x, y);
        }
      }
    } else if (s instanceof AR2.class) {
      for (let y = s.y & CELL_MASK; y < s.y + s.h; y += CELL_SIZE) {
        for (let x = s.x & CELL_MASK; x < s.x + s.w; x += CELL_SIZE) {
          func(x, y);
        }
      }
    } else if (s instanceof Poly2.class) {
      const r = s.aabb;
      for (let y = r.y & CELL_MASK; y < r.y + r.h; y += CELL_SIZE) {
        for (let x = r.x & CELL_MASK; x < r.x + r.w; x += CELL_SIZE) {
          if (s.intersects(AR2(x, y, CELL_SIZE, CELL_SIZE))) {
            func(x, y);
          }
        }
      }
    }
  }
  spritesIn(shape) {
    const sprites = new Set;
    Board.shapeEach(shape, (x, y) => {
      const cell = this.cell({x, y});
      if (cell) {
        for (const sprite of cell.sprites) {
          if (hitbox(sprite).intersects(shape)) sprites.add(sprite);
        }
      }
    });
    return sprites;
  }
  linkSprite(sprite) {
    Board.shapeEach(hitbox(sprite), (x, y) => {
      const cell = this.cellCreate({x, y});
      sprite.cells.push(cell);
      cell.sprites.push(sprite);
    });
  }
  unlinkSprite(sprite) {
    for (const cell of sprite.cells) {
      remove(cell.sprites, sprite);
      if (!cell.sprites.length) {
        this.deleteCell(cell.pos);
      }
    }
    sprite.cells = [];
  }
  createSprite(pos, type) {
    const sprite = {
      cells: [],
      pos,
      prev: pos.clone(),
      initial: {
        pos: pos.clone()
      },
      vel: V2(),
      type
    };
    this.linkSprite(sprite);
    this.sprites.get(type).push(sprite);
    if (SPRITES[type].enemy) this.enemies.push(sprite);
    if (SPRITES[type].pickup) this.pickups.push(sprite);
    return sprite;
  }
  moveSprite(sprite, pos) {
    this.unlinkSprite(sprite);
    sprite.pos = pos;
    this.linkSprite(sprite);
  }
  deleteSprite(sprite) {
    this.unlinkSprite(sprite);
    remove(this.sprites.get(sprite.type), sprite);
  }
  deactivateSprite(sprite) {
    this.deleteSprite(sprite);
    this.resetSprite(sprite);
    this.inactive.push(sprite);
  }
  activateSprite(sprite) {
    remove(this.inactive, sprite);
    const sprites = this.sprites.get(sprite.type);
    if (!sprites.includes(sprite)) {
      sprites.push(sprite);
      this.linkSprite(sprite);
    }
  }
  resetSprite(sprite) {
    sprite.vel = V2();
    for (const [key, value] of Object.entries(sprite.initial)) {
      if (value instanceof V2) {
        sprite[key] = sprite.initial[key].clone();
      } else {
        sprite[key] = sprite.initial[key];
      }
    }
  }
  respawn() {
    for (const sprite of this.enemies) {
      this.unlinkSprite(sprite);
      this.resetSprite(sprite);
      this.activateSprite(sprite);
    }
  }
  player() {
    return (this.sprites.get('player') || [])[0];
  }
}
