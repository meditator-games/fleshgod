
import { DEFAULT_FONT_FAMILY, DEFAULT_FONT_SIZE } from './defs.js';
import V2 from './util/vector2.js';

export default class CanvasBackend {
  constructor(ctx) {
    this.alpha = 1;
    this.z = 0;
    this.bg = 'black';
    this.ctx = ctx;
    this.canvas = ctx.canvas;
  }
  _identity() {
    const {ctx} = this;
    ctx.setTransform(1, 0, 0, 1, 0, 0);
  }
  setup(gfx) {
    this.quads = new Map();
    this.textures = {};
    for (const atlas of gfx) {
      for (const [name, {pos, size}] of Object.entries(atlas.rects)) {
        this.textures[name] = {
          image: atlas.image,
          pos: V2(...pos),
          size: V2(...size)
        };
      }
    }
  }
  drawUIQuad(img, pos, options = {}) {
    const tex = this.textures[img];
    const { alpha = this.alpha, z = 2, size = V2(tex.size.x, tex.size.y) } = options;
    const {ctx} = this;
    this.quads.set(z, this.quads.get(z) || []);
    this.quads.get(z).push(() => {
      const oldAlpha = ctx.globalAlpha;
      ctx.globalAlpha *= alpha;
      ctx.drawImage(tex.image, tex.pos.x, tex.pos.y, tex.size.x, tex.size.y, pos.x, pos.y, size.x, size.y);
      ctx.globalAlpha = oldAlpha;
    });
  }
  drawQuad(img, pos, options = {}) {
    if (img === 'empty') return;
    const tex = this.textures[img];
    const {
      transforms = [],
      size = V2(tex.size.x, tex.size.y),
      z = this.z,
      program = {}
    } = options;
    const { alpha = Object.prototype.hasOwnProperty.call(program, 'alpha') ? program.alpha : this.alpha } = options;
    const {ctx} = this;
    this.quads.set(z, this.quads.get(z) || []);
    this.quads.get(z).push(() => {
      const oldAlpha = ctx.globalAlpha;
      ctx.globalAlpha *= alpha;
      pos = pos.clone().subV2(this.camera);
      if (transforms.length) {
        ctx.translate(...pos.clone().addV2(size.clone().mulScalar(0.5)).array());
        pos = size.clone().mulScalar(-0.5);
        transforms.forEach(t => ({
          rotate: () => ctx.rotate(t.angle),
          flipX: () => ctx.scale(-1, 1),
          flipY: () => ctx.scale(1, -1),
          scale: () => ctx.scale(t.x, t.y)
        }[t.type || t]()));
      }
      ctx.drawImage(tex.image, tex.pos.x, tex.pos.y, tex.size.x, tex.size.y, pos.x, pos.y, size.x, size.y);
      if (transforms.length) {
        this._identity();
      }

      ctx.globalAlpha = oldAlpha;
    });
  }
  measureText(text, size = DEFAULT_FONT_SIZE, family = DEFAULT_FONT_FAMILY) {
   this.ctx.font = `${size}px ${family}`;
   return this.ctx.measureText(text);
  }
  fillRect({x, y}, {x: w, y: h}, {color, z = 1, alpha = this.alpha}) {
    const {ctx} = this;
    this.quads.set(z, this.quads.get(z) || []);
    this.quads.get(z).push(() => {
      const oldAlpha = ctx.globalAlpha;
      ctx.globalAlpha *= alpha;
      ctx.fillStyle = color;
      ctx.fillRect(x, y, w, h);
      ctx.globalAlpha = oldAlpha;
    });
  }
  strokeRect({x, y}, {x: w, y: h}, {color, z = 1, alpha = this.alpha}) {
    const {ctx} = this;
    this.quads.set(z, this.quads.get(z) || []);
    this.quads.get(z).push(() => {
      const oldAlpha = ctx.globalAlpha;
      ctx.globalAlpha *= alpha;
      ctx.strokeStyle = color;
      ctx.strokeRect(x, y, w, h);
      ctx.globalAlpha = oldAlpha;
    });
  }
  fillPath(func, {color, z = 1}) {
    const {ctx, alpha} = this;
    this.quads.set(z, this.quads.get(z) || []);
    this.quads.get(z).push(() => {
      const oldAlpha = ctx.globalAlpha;
      ctx.globalAlpha *= alpha;
      func();
      ctx.fillStyle = color;
      ctx.fill();
      ctx.globalAlpha = oldAlpha;
    });
  }
  drawText(text, { size = DEFAULT_FONT_SIZE, family = DEFAULT_FONT_FAMILY,
    y = 0, x = 0, z = 2, shadow = true, color='white' } = {}) {
    const {ctx} = this;
    this.quads.set(z, this.quads.get(z) || []);
    this.quads.get(z).push(() => {
      ctx.font = `${size}px ${family}`;
      if (shadow) {
        ctx.fillStyle = 'black';
        ctx.fillText(text, x + 4, y + 4);
      }
      ctx.font = `${size}px ${family}`;
      ctx.fillStyle = color;
      ctx.fillText(text, x, y);
    });
  }
  drawTextCentered(text, { size = DEFAULT_FONT_SIZE, family = DEFAULT_FONT_FAMILY,
    y = 0, x = 0, z = 2} = {}) {
    const metrics = this.measureText(text, size, family);
    this.drawText(text, {
      x: (this.canvas.width - metrics.width) / 2 + x,
      y: (this.canvas.height + size) / 2 + y,
      z,
      size
    });
  }
  newFrame({ camera }) {
    this.camera = camera;
    this._identity();
  }
  endFrame() {
    const {ctx} = this;
    ctx.fillStyle = this.bg;
    ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);
    const layers = [...this.quads.keys()];
    layers.sort((a, b) => a - b);
    for (const layer of layers) {
      for (const f of this.quads.get(layer)) f();
    }
    this.quads.clear();
    this.alpha = 1;
    this.z = 0;
  }
}

