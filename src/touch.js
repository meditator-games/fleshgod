import AR2 from './util/arect2.js';

export default function checkTouch(touchPos, buttons) {
  for (const [, button] of Object.entries(buttons)) {
    const {pos, radius, size, angles} = button;
    if (size) {
      if (AR2.fromPointAndSize(pos, size).contains(touchPos)) {
        return button;
      }
    } else if (radius) {
      const relpos = pos.clone().subV2(touchPos);
      if (relpos.length() < radius) {
        if (angles) {
          const cr = Math.atan2(relpos.x, relpos.y);
          const [ra, rb] = angles;
          if (ra < rb) {
            if (cr >= ra && cr <= rb) {
              return button;
            }
          } else if (cr >= ra || cr <= rb) {
            return button;
          }
        } else {
          return button;
        }
      }
    }
  }
  return null;
}

