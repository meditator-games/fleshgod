
import V2 from './util/vector2.js';
import {deepFreeze} from './util/misc.js';

const COMPONENTS = deepFreeze({
  rotate: {
    rot: 0,
    rvel: 0
  }
});

const SPRITES = deepFreeze({
  player: {
    radius: 4,
    // size: V2(1, 1),
    setup: () => ({
      ...COMPONENTS.rotate,
      hp: 6,
      speed: 2,
      score: 0,
      weapon: 0,
      shield: 0,
      powerMeter: 0,
      invis: 0
    })
  },
  enemy: {
    radius: 10,
    // size: V2(8, 9),
    enemy: true,
    value: 5,
    setup: () => ({
      ...COMPONENTS.rotate,
      hp: 1,
      target: null,
      turnTarget: null,
      firingTimer: 0
    })
  },
  powerup: {
    radius: 13,
    // size: V2(13, 16),
    pickup: true,
    value: 10,
    setup: () => ({
      anim: 0
    })
  },
  bullet: {
    size: V2(2, 5),
    particle: true,
    setup: () => ({
      ...COMPONENTS.rotate,
      skin: 'player',
      parent: null
    })
  },
  boss: {
    radius: 63,
    enemy: true,
    value: 500,
    setup: (sprite, game) => ({
      ...COMPONENTS.rotate,
      deathAnim: 0,
      hp: 352,
      anger: 0,
      target: null,
      turnTimer: 0,
      tail: Array(3).fill().map((k, i) =>
        game.createSprite(sprite.pos, 'meatball', { scale: 0.45 - i*0.075, tail: i + 1 }))
    })
  },
  meatball: {
    radius: 127,
    setup: () => ({
      scale: 1
    })
  },
  particle: {
    radius: 1,
    // size: V2(1, 1),
    particle: true,
    setup: () => ({
      anim: 0,
      lifetime: 30
    })
  },
  explosion: {
    radius: 31,
    lifetime: 4 * 9,
    particle: true,
    setup: () => ({
      anim: 0,
      scale: 1
    })
  }
});

const FRAME_RATE = 60;
const TICK_LENGTH = 1000 / FRAME_RATE;
const SCREEN_SIZE = Object.freeze(V2(480, 480));
const CELL_SHIFT = 6;
const CELL_SIZE = 1 << CELL_SHIFT;
const CELL_MASK = ~(CELL_SIZE - 1);

const DEFAULT_FONT_FAMILY = 'ArcadePix';
const DEFAULT_FONT_SIZE = 32;

export {
  SPRITES,
  CELL_SIZE,
  CELL_SHIFT,
  CELL_MASK,
  FRAME_RATE,
  TICK_LENGTH,
  SCREEN_SIZE,
  DEFAULT_FONT_SIZE,
  DEFAULT_FONT_FAMILY
};
