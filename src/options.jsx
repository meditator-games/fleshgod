import V2 from './util/vector2.js';
import {createElement} from './framework.js';

export default function createOptionsMenu({ui, config}) {
  return ui.bus.tag('optionsToggled', () =>
    <div class="menu-overlay" onClick={ui.closeOptions} style={{display: ui.options ? '' : 'none'}}>
      <div class="dialog menu-options" onClick={e => e.stopPropagation()}
           style={{display: ui.options ? '' : 'none', top: ui.project(V2()).toStyle().top}}>
        <header><span>Options</span><button class="close-button" onClick={ui.closeOptions}></button></header>
        <div class="menu-table">
          <label for="sfxVolume">SFX Volume</label>
            <input type="range" id="sfxVolume" min="0" max="1" step="0.01" value={config.sfxVolume} onInput={event => {
              config.sfxVolume = parseFloat(event.target.value);
              ui.save();
              ui.reloadSound();
            }} />
          <label for="musicVolume">Music Volume</label>
            <input type="range" id="musicVolume" min="0" max="1" step="0.01" value={config.musicVolume} onInput={event => {
              config.musicVolume = parseFloat(event.target.value);
              ui.save();
              ui.reloadSound();
            }} />
          {ui.webglSupported
          ? <label for="glToggle">Hardware Rendering</label> : null}
          {ui.webglSupported
          ? <input type="checkbox" id="glToggle" checked={config.gl} onChange={event => {
              config.gl = event.target.checked;
              ui.save();
              ui.bus.emit('glToggled');
              ui.reloadCanvas();
            }} /> : null}
            <label for="postProcessingToggle">Post-Processing</label>
            <input type="checkbox" id="postProcessingToggle" checked={config.postProcessing} onChange={event => {
              config.postProcessing = event.target.checked;
              ui.save();
              ui.redraw();
            }} />
        </div>
        <button type="button" onClick={ui.openCredits}>Show Credits</button>
      </div>
    </div>);
}
