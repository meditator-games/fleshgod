import AR2 from './util/arect2.js';
import Circ from './util/circle.js';
import Poly2 from './util/polygon2.js';
import V2 from './util/vector2.js';
import {SPRITES} from './defs.js';

const defaultCollide = ({type}) =>
  !SPRITES[type].particle && !SPRITES[type].pickup;
const COLLISION = Object.freeze({
  player: defaultCollide,
  bullet: defaultCollide,
  enemy: defaultCollide
});

function blocking(sprite, other) {
  return COLLISION[sprite.type](other);
}

function hitbox(sprite) {
  const {pos, type, scale=1, rot} = sprite;
  const {size, radius, offset=V2(), hitbox: custom} = SPRITES[type];
  if (custom) {
    return custom(sprite);
  }
  if (size) {
    let rect = AR2.fromPointAndSize(pos, size);
    if (scale !== 1 || rot) {
      const centerOffset = pos.clone().addV2(size.clone().mulScalar(0.5)).addV2(offset);
      rect.translate(centerOffset.mulScalar(-1));
      if (scale !== 1) {
        rect.scale(V2(scale, scale));
      }
      if (rot) {
        rect = rect.rotated(rot);
      }
      rect.translate(centerOffset.mulScalar(-1));
    }
    return rect;
  }
  if (radius) {
    return Circ(pos.x, pos.y, radius * scale).translate(offset);
  }
  return Poly2();
}

export {blocking, hitbox};
