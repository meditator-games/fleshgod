import {CELL_MASK, CELL_SIZE, DEFAULT_FONT_SIZE, FRAME_RATE, SCREEN_SIZE, SPRITES, TICK_LENGTH} from './defs.js';
import V2 from './util/vector2.js';
import {deepFreeze} from './util/misc.js';
import { mat3 } from 'gl-matrix';

export default class Renderer {
  constructor({game, system, now, config}) {
    this.game = game;
    this.config = config;
    this.system = system;
    this.draw = new Draw(this);
    this.last = now;
    this.paused = false;
  }
  frame(ctx, now) {
    const {draw, game} = this;
    if (game.mode === 'pause' && this.paused) {
      return;
    }
    this.paused = false;
    let camera = null;
    game.cameraShake = Math.min(game.cameraShake, 0.5);
    if (game.mode === 'title')
      camera = game.camera.clone();
    else
      camera = game.camera.clone()
        .add(game.cameraShake * 32 * (Math.random() * 2 - 1),
             game.cameraShake * 32 * (Math.random() * 2 - 1))
    camera = camera.map(v => Math.round(v));
    const camup = camera.clone().add(ctx.canvas.width, ctx.canvas.height);

    ctx.newFrame({ camera });
    draw.start({ctx, camera, now});

    const STATUSH = 16;
    const STATUSY = SCREEN_SIZE.y - STATUSH*2;
    const centerAlignText = (str, props) => {
      const {width} = ctx.measureText(str, props.size || DEFAULT_FONT_SIZE);
      props.x -= width / 2;
      ctx.drawText(str, props);
      return width;
    };
    const leftAlignText = (str, props) => {
      const {width} = ctx.measureText(str, props.size || DEFAULT_FONT_SIZE);
      props.x -= width;
      ctx.drawText(str, props);
      return width;
    };
    if (game.mode === 'title') {
      ctx.drawQuad('title', SCREEN_SIZE.clone().subV2(ctx.textures.title.size).mulScalar(0.5).sub(0, 96), {
        alpha: Math.sin(game.timer / 90 * Math.PI * 2) * 0.05 + 0.9
      });
      const INSTRUCTIONS = Object.freeze([
        'INSTRUCTIONS:',
        'WASD - MOVE',
        'MOUSE - AIM',
        'SPACE/LEFTCLICK - FIRE',
        'Q/RIGHTCLICK - APPLY SPAM',
        '',
        'EACH SPAM YOU COLLECT',
        'ADVANCES YOUR POWER METER.',
        'APPLY YOUR SPAM POWERS ONCE',
        'YOUR POWER METER HAS REACHED',
        'THE DESIRED LEVEL.',
        '',
        'CAN YOU DEFEAT THE FLESHGOD',
        'AND CLAIM THE HIGH SCORE?'
      ]);
      const SX = SCREEN_SIZE.x / 2 - 176;
      for (let i = 0; i < INSTRUCTIONS.length; i += 1) {
        ctx.drawText(INSTRUCTIONS[i], {x: SX, y: SCREEN_SIZE.y/2 + 16*i, size: 16});
      }
    } else if (game.mode === 'game' || game.mode === 'pause') {
      const drawBg = (scale, next=scale/2) => {
        const bgStart = camera.clone().mulScalar(scale);
        const bgEnd = bgStart.clone().add(ctx.canvas.width, ctx.canvas.height);
        for (let y = (bgStart.y & CELL_MASK) - CELL_SIZE; y < bgEnd.y + CELL_SIZE; y += CELL_SIZE) {
          for (let x = (bgStart.x & CELL_MASK) - CELL_SIZE; x < bgEnd.x + CELL_SIZE; x += CELL_SIZE) {
            const xo = game.constructor.whiteNoise(x / 2, y / 2, 0.76383) * 32;
            const yo = game.constructor.whiteNoise(x / 2, y / 2, 0.63426) * 32;
            const zo = game.constructor.whiteNoise(x / 2, y / 2, 0.42522) * 0.5 + 0.5;
            draw.star({
              //pos: V2(x + xo, y + yo).subV2(camera.clone().mulScalar(zo * 0.5 + 0.25)).addV2(camera),
              pos: V2(x + xo, y + yo).subV2(bgStart).addV2(camera),
              alpha: (zo * (scale - next) + next) * 2,
              z: scale,
              scale: 0.5
            });
          }
        }
      };
      drawBg(1/16);
      drawBg(1/8);
      drawBg(1/4);
      drawBg(1/2);
      const drawn = new Set();
      for (let y = (camera.y & CELL_MASK) - CELL_SIZE; y < camup.y + CELL_SIZE; y += CELL_SIZE) {
        for (let x = (camera.x & CELL_MASK) - CELL_SIZE; x < camup.x + CELL_SIZE; x += CELL_SIZE) {
          const cell = this.game.board.cell({x, y});
          if (cell) {
            for (const sprite of cell.sprites) {
              if (!drawn.has(sprite)) {
                draw.sprite(sprite);
                drawn.add(sprite);
              }
            }
          }
        }
      }
      const player = game.player();
      if (player) {
        centerAlignText('1UP',
                      { x: SCREEN_SIZE.x - STATUSH - 224, y: STATUSH * 1.5, size: STATUSH, z: 64 });
        centerAlignText(`${player.score}`,
                      { x: SCREEN_SIZE.x - STATUSH - 224, y: STATUSH * 2.5, size: STATUSH, z: 64 });
        const powerMeter = [
          player.weapon < 2 ? 'WEAPON' : null,
          player.speed < 4 ? 'SPEED': null,
          player.shield < 2 ? 'SHIELD': null,
          'HP'
        ];
        if (!this.powerMeterMeasure) {
          this.powerMeterMeasure = powerMeter.map(
            str => str && ctx.measureText(str, STATUSH/2));
        }
        const CELL_WIDTH = 48;
        for (let i = 0; i < powerMeter.length; i += 1) {
          const pt = powerMeter[i];
          const pos = V2(STATUSH + i * CELL_WIDTH, STATUSY);
          const alpha = i + 1 === player.powerMeter ? 1 : 1/8;
          ctx.strokeRect(pos,
                       V2(CELL_WIDTH, STATUSH), {color: '#ffffff', alpha, z: 64});
          if (pt) {
            const textColor = `rgba(255, 255, 255, ${alpha}`;
            ctx.drawText(pt, {
              x: pos.x + (CELL_WIDTH - this.powerMeterMeasure[i].width) / 2,
              y: pos.y + STATUSH*0.75,
              size: STATUSH/2,
              z: 64,
              shadow: false,
              color: textColor
            });
          }
        }
        const HP_WIDTH = 128;
        ctx.fillRect(V2(SCREEN_SIZE.x - STATUSH * 2 - HP_WIDTH, STATUSY),
                     V2(HP_WIDTH * (player.hp / 12), STATUSH), {color: '#ff0000', alpha: 0.8, z: 64});
        ctx.strokeRect(V2(SCREEN_SIZE.x - STATUSH * 2 - HP_WIDTH, STATUSY),
                       V2(HP_WIDTH, STATUSH), {color: '#ffffff', alpha: 0.8, z: 64});
      } else if (game.gameOverTimer < 60) {
        ctx.drawTextCentered('GAME OVER', { z: 64 });
        ctx.drawTextCentered(`SCORE: ${game.finalScore}`, { y: 48, z: 64 });
      }
    }
    centerAlignText('HIGH SCORE',
                  { x: SCREEN_SIZE.x - STATUSH - 64, y: STATUSH * 1.5, size: STATUSH, z: 64 });
    centerAlignText(`${game.highScore}`,
                  { x: SCREEN_SIZE.x - STATUSH - 64, y: STATUSH * 2.5, size: STATUSH, z: 64 });
    if(game.mode === 'pause') {
      ctx.drawTextCentered('PAUSE', { z: 64 });
      this.paused = true;
    }
    ctx.endFrame({
      separation: new Float32Array(
        Array(6).fill().map(() => game.cameraShake * 32 * (Math.random() * 2 - 1)))
    });
    game.cameraShake = Math.max(0, game.cameraShake - (now - this.last) / 1000);
    this.last = now;
  }
}


class Draw {
  constructor({game}) {
    this.game = game;
  }
  start({ctx, camera, now}) {
    this.ctx = ctx;
    this.camera = camera;

    this.now = now;
    const t = now % 1000 / 1000;
    const col = 128 + Math.floor(Math.sin(t * 2 * Math.PI) * 64 + 64);
    this.selectShade = col.toString(16).padStart(2, '0');
    this.dt = (this.now - this.game.now) / 1000 / TICK_LENGTH;
  }
  sprite(s) {
    this[s.type](s);
  }
  rotatedQuad({ pos, vel, rot, rvel }, tex, offset, extra = {}) {
    const p = pos.clone().addV2(vel.clone().mulScalar(this.dt)).subV2(offset);
    this.ctx.drawQuad(tex, p, {
      ...extra,
      transforms: [
        {type: 'rotate', angle: Math.PI * 0.5 + rot + rvel * this.dt}
      ]
    });
  }
  player(sprite) {
    this.rotatedQuad(sprite, 'player', V2(8, 7), {alpha: sprite.invis > 0 ? 0.25 : 1, z: 1});
  }
  enemy(sprite) {
    this.rotatedQuad(sprite, 'enemy', V2(12, 6));
  }
  powerup({ pos, vel }) {
    const p = pos.clone().addV2(vel.clone().mulScalar(this.dt)).sub(8, 9);
    this.ctx.drawQuad('powerup', p);
  }
  bullet(sprite) {
    this.rotatedQuad(sprite, `${sprite.skin}-shot`, V2(1, 3));
  }
  boss({ pos, vel, rot, rvel }) {
    const p = pos.clone().addV2(vel.clone().mulScalar(this.dt));
    this.ctx.drawQuad('boss', p.clone().sub(63, 63), {z: 3});
    const t = rot + rvel * this.dt;
    this.rotatedQuad(
      { pos: p.clone().addV2(V2(-24, -24).rotate(t)), vel, rot: rot - Math.PI / 2, rvel },
      'eyeball', V2(13, 13), {z: 3.5});
    this.rotatedQuad(
      { pos: p.clone().addV2(V2(24, -24).rotate(t)), vel, rot: rot - Math.PI / 2, rvel },
      'eyeball', V2(13, 13), {z: 3.5});
  }
  meatball({pos, vel, scale, tail}) {
    let tex = null;
    if (scale <= 0.25) {
      tex = 'meatball-1';
    } else if (scale <= 0.5) {
      tex = 'meatball-2';
    } else {
      tex = 'meatball-3';
    }
    const radius = SPRITES.meatball.radius * scale;
    const p = pos.clone().addV2(vel.clone().mulScalar(this.dt)).subScalar(radius);
    if (scale <= 0.25) {
      scale /= 0.25;
    } else if (scale <= 0.5) {
      scale /= 0.5;
    }
    this.ctx.drawQuad(tex, p, {
      z: tail ? (2 - (tail*0.01)) : 4 + (1/1024)*(p.y-this.camera.y),
      transforms: [
        {type: 'scale', x: scale, y: scale}
      ]
    });
  }
  star({pos, alpha, scale}) {
    const p = pos.clone().sub(1, 1);
    this.ctx.drawQuad('particle', p, { alpha, z: 0,
          transforms: [{type: 'scale', x: scale, y: scale}]});
  }
  particle({pos, vel, anim, lifetime}) {
    anim += this.dt;
    const alpha = 1 - anim / lifetime;
    const p = pos.clone().addV2(vel.clone().mulScalar(this.dt)).sub(1, 1);
    this.ctx.drawQuad('particle', p, { alpha, z: 0.9 });
  }
  explosion({pos, anim, scale}) {
    anim += this.dt;
    const {radius, lifetime} = SPRITES.explosion;
    if (anim < lifetime) {
      this.ctx.drawQuad(`explosion-${Math.floor(anim / (lifetime/9))+1}`,
        pos.clone().subScalar(radius), {
          transforms: [{type: 'scale', x: scale, y: scale}],
          z: 3.6
        });
    }
  }
}
