import { remove } from './util/misc.js';

export { createElement, Bus, VDOM, HTML };

const DEBUG = false;

const SVG_ELEMS = ['altGlyph', 'altGlyphDef', 'altGlyphItem', 'animate',
'animateColor', 'animateMotion', 'animateTransform', 'circle', 'clipPath',
'color-profile', 'cursor', 'defs', 'desc', 'discard', 'ellipse', 'feBlend',
'feColorMatrix', 'feComponentTransfer', 'feComposite', 'feConvolveMatrix',
'feDiffuseLighting', 'feDisplacementMap', 'feDistantLight', 'feDropShadow',
'feFlood', 'feFuncA', 'feFuncB', 'feFuncG', 'feFuncR', 'feGaussianBlur',
'feImage', 'feMerge', 'feMergeNode', 'feMorphology', 'feOffset',
'fePointLight', 'feSpecularLighting', 'feSpotLight', 'feTile', 'feTurbulence',
'filter', 'font', 'font-face', 'font-face-format', 'font-face-name',
'font-face-src', 'font-face-uri', 'foreignObject', 'g', 'glyph', 'glyphRef',
'hatch', 'hatchpath', 'hkern', 'image', 'line', 'linearGradient', 'marker',
'mask', 'mesh', 'meshgradient', 'meshpatch', 'meshrow', 'metadata',
'missing-glyph', 'mpath', 'path', 'pattern', 'polygon', 'polyline',
'radialGradient', 'rect', 'script', 'set', 'solidcolor', 'stop', 'style',
'svg', 'switch', 'symbol', 'text', 'textPath', 'title', 'tref', 'tspan',
'unknown', 'use', 'view', 'vkern'];

const BOOLEAN_ATTRIBUTES = [ 'async', 'autofocus', 'autoplay', 'checked',
'compact', 'controls', 'default', 'defaultChecked', 'defaultMuted', 'defer',
'disabled', 'formNoValidate', 'hidden', 'indeterminate', 'isMap', 'itemscope',
'loop', 'multiple', 'muted', 'noResize', 'noValidate', 'readOnly', 'required',
'reversed', 'selected', 'spellCheck', 'translate', 'typeMustMatch'];

const NAMESPACES = {
  svg: 'http://www.w3.org/2000/svg',
  xlink: 'http://www.w3.org/1999/xlink'
};

let counter = 0;
class VNode {
  constructor(key = null) {
    if (DEBUG) this.id = counter++;
    this.key = key;
  }
}

class VElement extends VNode {
  constructor(key = null) {
    super(key);
    this.children = [];
    this.keys = new Map;
  }
  setChildren(children) {
    for (let i = 0; i < children.length; i += 1) {
      const cd = children[i];
      if (Array.isArray(cd)) {
        children.splice(i, 1, ...cd);
        i -= 1;
        continue;
      }
      if (typeof cd === 'undefined' || cd === null) {
        continue;
      }
      const child = cd instanceof VNode ? cd : new VText(`${cd}`);
      this.children[i] = child;
      if (typeof child.key !== 'undefined' && child.key !== null) {
        if (this.keys.has(child.key)) {
          console.log('WARNING: duplicate key on VNode: ', child);
        }
        this.keys.set(child.key, child);
      }
    }
  }
}

class VRealElement extends VElement {
  constructor(type, attr, key = null) {
    super(key);
    this.type = type;
    this.attr = attr || {};
  }
  toString() {
    return `[VRealElement type:${this.type}]`;
  }
}

class VBinding {
  constructor(event, func, bus) {
    this.event = event;
    this.func = func;
    this.bus = bus;
  }
}

class VText extends VNode {
  constructor(value, key = null) {
    super(key);
    this.value = `${value}`;
  }
  toString() {
    return `[VText value:${this.value}]`;
  }
}

class VMarkup extends VNode {
  constructor(value, key = null) {
    super(key);
    this.value = value;
  }
  toString() {
    return `[VMarkup value:${this.value}]`;
  }
}

class VTagElement extends VElement {
  constructor(event, func, bus, key = null) {
    super(key);
    this.binding = new VBinding(event, func, bus);
  }
  toString() {
    return `[VTagElement event:${this.binding.event}]`;
  }
}

function createElement(type, attr, ...children) {
  const def = new VRealElement(type, attr);
  def.setChildren(children);
  if (DEBUG) console.log('created element', def);
  return def;
}

function HTML(markup) {
  const def = new VMarkup(markup);
  if (DEBUG) console.log('created element', def);
  return def;
};

function setAttribute_(elem, key, value) {
  if (key.substr(0, 2) == 'on') {
    const eventName = key.substr(2).toLowerCase();
    if (elem.events[eventName] !== value) {
      if (elem.events[eventName]) {
        elem.removeEventListener(eventName, elem.events[eventName]);
      }
      elem.events[eventName] = value;
      elem.addEventListener(eventName, value);
    }
  } else {
    const [, ns] = (key.match(/^(?:(.+):)?(.+)$/) || []);
    if (ns) {
      elem.setAttributeNS(NAMESPACES[ns], key, value);
    } else if (key === 'value' && (elem instanceof HTMLInputElement || elem instanceof HTMLSelectElement || elem instanceof HTMLTextAreaElement)) {
      elem.value = value;
    } else if (key === 'style') {
      Object.assign(elem.style, value);
    } else if (BOOLEAN_ATTRIBUTES.includes(key)) {
      elem[key] = value;
    } else {
      elem.setAttribute(key, value);
    }
  }
}

class VDOM {
  constructor(document = window.document) {
    this.document = document;
    this.elements = new WeakMap; // WeakMap<VNode, Node>
  }

  update_(def, old, parent) {
    const {elements} = this;
    if (DEBUG) console.log('update', old, 'to', def, 'on parent', parent);
    const elem = elements.get(old);
    if (def instanceof VTagElement) {
      def.parent = parent;
    }
    if (Object.getPrototypeOf(def) !== Object.getPrototypeOf(old)
      || (def instanceof VRealElement && def.type !== old.type)) {
      if (old instanceof VTagElement || def instanceof VTagElement) {
        const oldList = old instanceof VTagElement ? old.children : [old];
        const newList = def instanceof VTagElement ? def.children : [def];
        if (oldList === old.children) {
          Bus.unbind(old);
        }
        const elems = this.updateList_(newList, oldList, parent, old instanceof VElement && old.keys);
        return [elems];
      }
      if (!elem) {
        throw new Error(`no element for ${old}`);
      }
      const [newElem] = this.render(def, parent);
      if (DEBUG) console.log('rendered', def, 'to parent', parent);
      if (elem instanceof DocumentFragment) {
        console.log(elem);
        debugger;
      }
      parent.replaceChild(newElem, elem);
      elements.delete(old);
      Bus.unbind(old);
      elements.set(def, newElem);
      return [newElem];
    }

    if (def instanceof VTagElement) {
      const elems = this.updateList_(def.children, old.children, parent, old.keys);
      Bus.unbind(old);
      return elems;
    }

    if (!elem) {
      throw new Error(`no element for ${old}`);
    }
    if (def instanceof VText) {
      if (def.value !== old.value) {
        elem.nodeValue = def.value;
      }
    } else if (def instanceof VMarkup) {
      if (def.value !== old.value) {
        for (const cd of [...elem.childNodes.values()]) {
          elem.removeChild(cd);
        }
        const temp = this.document.createElement('body');
        temp.innerHTML = def.value;
        for (const cd of [...temp.childNodes.values()]) {
          elem.appendChild(cd);
        }
      }
    } else if (def instanceof VRealElement) {
      this.updateList_(def.children, old.children, elem, old.keys);
      const newKeys = Object.keys(def.attr || {});
      for (const key of newKeys) {
        if (def.attr[key] !== old.attr[key]) {
          setAttribute_(elem, key, def.attr[key]);
        }
      }
      for (const key of Object.keys(old.attr || {})) {
        if (!newKeys.includes(key)) {
          const eventName = EVENTS.find(e => `on${e}` === key);
          if (eventName) {
            elem[key.toLowerCase()] = null;
          } else if (key === 'value') {
            elem.value = undefined;
          } else if (key === 'style') {
            elem.style = {};
          } else {
            elem.removeAttribute(key);
          }
        }
      }
    }
    Bus.unbind(old);
    elements.delete(old);
    elements.set(def, elem);
    return [elem];
  }
  updateList_(curr, prev, parent, keys) {
    if (DEBUG) console.log('update list', prev, 'to', curr, 'on parent', parent);
    const newChildren = [...(curr || [])];
    const oldChildren = [...(prev || [])];
    const totalChildren = Math.max(newChildren.length, oldChildren.length);
    const deleted = [];
    const added = [];
    const firstOld = oldChildren[oldChildren.length - 1];
    let next = (firstOld && this.elements.get(firstOld) || {}).nextSibling;
    for (let i = totalChildren - 1; i >= 0; i -= 1) {
      const newChild = newChildren[i];
      const key = newChild && typeof newChild.key !== 'undefined' && newChild.key !== null ? newChild.key : null;
      const oldChild = key === null ? oldChildren[i] : (keys && keys.get(key));
      if (newChild && oldChild) {
        const children = this.update_(newChild, oldChild, parent);
        added.unshift(...children);
        if (children[0]) {
          next = children[0];
        }
      } else if (newChild) {
        const children = this.render(newChild).reverse();
        if (newChild instanceof VTagElement) {
          newChild.parent = parent;
        }
        for (const child of children) {
          if (child instanceof VTagElement) {
            child.parent = parent;
          }
          if (next) {
            parent.insertBefore(child, next);
          } else {
            parent.appendChild(child);
          }
          added.unshift(child);
          next = child;
        }
      } else if (oldChild) {
        deleted.push(oldChild);
      }
    }
    while (deleted.length) {
      const d = deleted.slice();
      deleted.splice(0);
      for (const child of d) {
        Bus.unbind(child);
        if (child instanceof VTagElement) {
          deleted.push(...child.children);
        } else {
          if (DEBUG) console.log('delete elem for', child);
          parent.removeChild(this.elements.get(child));
          this.elements.delete(child);
        }
      }
    }
    return added;
  }

  render(def, parent) {
    let elem = null;
    if (!parent) {
      parent = this.document.createDocumentFragment();
    }
    if (DEBUG) console.log('rendering', def, 'to parent', parent);
    if (def instanceof VText) {
      elem = this.document.createTextNode(def.value);
    } else if (def instanceof VMarkup) {
      let container;
      if (parent instanceof DocumentFragment) {
        container = parent;
      } else {
        container = this.document.createDocumentFragment();
      }
      const temp = this.document.createElement('body');
      temp.innerHTML = def.value;
      for (const cd of [...temp.childNodes.values()]) {
        container.appendChild(cd);
      }
      this.elements.set(def, container);
      if (container instanceof DocumentFragment && parent != container) {
        parent.appendChild(container);
      }
      return [container];
    } else if (def instanceof VTagElement) {
      def.parent = parent;
      return def.children.reduce((cs, c) => [...cs, ...(c ? this.render(c, parent) : [])], []);
    } else if (def instanceof VRealElement) {
      const {type, attr = {}, children = []} = def;
      if (SVG_ELEMS.includes(type)) {
        elem = this.document.createElementNS(NAMESPACES.svg, type);
      } else {
        elem = this.document.createElement(type);
      }
      elem.events = {};
      for (const c of children) {
        if (c) {
          this.render(c, elem);
        }
      }
      for (const key of Object.keys(attr)) {
        setAttribute_(elem, key, attr[key]);
      }
    }
    if (elem) {
      if (DEBUG) console.log('set elem', def, '=', elem);
      this.elements.set(def, elem);
      parent.appendChild(elem);
      return [elem];
    }
    return [];
  }
}

class Bus {
  constructor(vdom) {
    this.vdom = vdom;
    this.events = new Map; // Map<String, VTagElement[]>
  }

  dispatch(event, callback) {
    return e => this.emit(event, callback(e));
  }

  emit(event, ...args) {
    const {vdom} = this;
    const bindings = this.events.get(event) || [];
    for (const def of [...bindings]) {
      if (def instanceof VTagElement) {
        const parent = def.parent || (vdom.elements.get(def) || {}).parentNode;
        const prev = def.children;
        let curr = def.binding.func(...args);
        if (!Array.isArray(curr)) curr = [curr];
        for (let i = 0; i < curr.length; i++) {
          const child = curr[i];
          if (typeof child === 'undefined' || child === null) {
            continue;
          }
          if (!(child instanceof VNode)) {
            curr[i] = new VText(`${child}`);
          }
        }
        vdom.updateList_(curr, prev, parent);
        prev.splice(0, prev.length, ...curr);
      } else if (def instanceof VBinding) {
        def.func(...args);
      }
    }
  }
  bind(event, func) {
    const binding = new VBinding(event, func, this);
    this.events.set(event, [...(this.events.get(event) || []), binding]);
    if (DEBUG) console.log('created binding', binding);
    return binding;
  }
  static unbind(def) {
    if (DEBUG) console.log('unbind', def);
    if (def instanceof VTagElement || def instanceof VBinding) {
      const {bus, event} = def instanceof VBinding ? def : def.binding;
      const bindings = bus.events.get(event);
      if (bindings) {
        remove(bindings, def);
        if (!bindings.length) {
          if (DEBUG) console.log('unbound last ', event);
          bus.events.delete(event);
        }
      }
    }
    if (def instanceof VElement) {
      for (const c of def.children) {
        if (c) Bus.unbind(c);
      }
    }
  }
  tag(event, func, ...initial) {
    const children = func(...initial);
    const binding = new VTagElement(event, func, this);
    binding.setChildren(Array.isArray(children) ? children : [children]);
    this.events.set(event, [...(this.events.get(event) || []), binding]);
    if (DEBUG) console.log('created tag binding', binding);
    return binding;
  }
}

