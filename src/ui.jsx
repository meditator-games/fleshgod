
import {Bus, createElement} from './framework.js';
import V2 from './util/vector2.js';
import {bindAll} from './util/misc.js';
import createCredits from './credits.jsx';
import createOptionsMenu from './options.jsx';

export default class UI {
  constructor(window, vdom) {
    this.options = false;
    this.title = true;
    this.credits = false;
    this.bus = new Bus(vdom);
    this.editor = {
      show: false
    };
    this.webglSupported = (() => {
      try {
        const c = window.document.createElement('canvas');
        const gl = c.getContext('webgl');
        if (!gl) return false;
        const maxTextureSize = gl.getParameter(gl.MAX_TEXTURE_SIZE);
        if (maxTextureSize < 2048) return false;
        return true;
      } catch (e) {}
      return false;
    })();
    bindAll(this);
  }
  render(config) {
    this.config = config;
    return this.bus.tag('resized', () => this.system
     ? [<section class="toolbar editor-main-toolbar" style={this.project(V2()).add(4, 4).toStyle()}>
          <button type="button" onClick={this.openOptions} title="Options">
            <img src="ui/cog.svg" />
          </button>
        </section>,
        createOptionsMenu({ui: this, config}),
        createCredits({ui: this})
      ] : null);
  }
  back() {
    if (this.system) {
      this.system.events.push({type: 'keydown', code: 'Escape'});
    }
  }
  openOptions() {
    this.options = true;
    this.bus.emit('optionsToggled');
  }
  closeOptions() {
    if (this.system) {
      this.system.poll();
    }
    this.options = false;
    this.credits = false;
    this.bus.emit('optionsToggled');
    this.bus.emit('creditsToggled');
  }
  openCredits() {
    this.credits = true;
    this.bus.emit('creditsToggled');
  }
  closeCredits() {
    if (this.system) {
      this.system.poll();
    }
    this.credits = false;
    this.bus.emit('creditsToggled');
  }
  blocking() {
    return this.options;
  }

  resized() {
    this.bus.emit('resized');
  }
}
