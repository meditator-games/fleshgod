import * as Sound from './sound.js';
import {CELL_MASK, CELL_SIZE, SCREEN_SIZE, SPRITES, TICK_LENGTH} from './defs.js';
import {blocking, hitbox} from './physics.js';
import {randInt, randRange, randItem} from './util/random.js';
import AR2 from './util/arect2.js';
import Alea from './util/alea.js';
import Board from './board.js';
import V2 from './util/vector2.js';
import checkTouch from './touch.js';
import {deepFreeze, removeAll} from './util/misc.js';

function clamp(a, min, max) {
  return Math.max(Math.min(a, max), min);
}

const INVALID_KEYS = Object.freeze(new Set([
  'ControlLeft', 'AltLeft', 'ShiftLeft', 'MetaLeft',
  'ControlRight', 'AltRight', 'ShiftRight', 'MetaRight',
  'ArrowUp', 'ArrowDown', 'ArrowLeft', 'ArrowRight'
]));
const THRUST_DECAY_TICKS = 10;
const SOUND_THRESHOLD = SCREEN_SIZE.x;
const GRID_SPACING = 128;

const normalizeAngle = angle => {
  while (angle <= -Math.PI) {
    angle += Math.PI * 2;
  }
  while (angle > Math.PI) {
    angle -= Math.PI * 2;
  }
  return angle;
};

export default class Game {
  constructor({window, canvas, system, sounds, audio, config, ui}) {
    this.ui = ui;
    this.window = window;
    this.canvas = canvas;
    this.system = system;
    this.sounds = sounds;
    this.audio = audio;
    this.config = config;
    this.soundChannels = {};
    this.timer = 0;
    this.board = new Board();
    this.random = new Alea();
    this.camera = V2();
    this.cameraShake = 0;
    this.globalTimer = 0;
    this.highScore = 3000;
    this.showHitboxes = false;
    this.keybinds = Object.freeze({
      KeyW: () => this.ymove.unshift(-1),
      KeyD: () => this.xmove.unshift(1),
      KeyS: () => this.ymove.unshift(1),
      KeyA: () => this.xmove.unshift(-1),
      Enter: () => this.pause(),
      Escape: () => this.quit(),
      Space: () => this.fire(),
      KeyQ: () => this.usePowerup(),
      KeyH: () => PRODUCTION ? 0 : this.toggleHitboxes()
    });
    this.keyups = Object.freeze({
      KeyW: () => removeAll(this.ymove, -1),
      KeyS: () => removeAll(this.ymove, 1),
      KeyD: () => removeAll(this.xmove, 1),
      KeyA: () => removeAll(this.xmove, -1),
      Space: () => this.stopFire(),
    });
    this.touchButtons = {};
    this.xmove = [];
    this.ymove = [];
    this.joyLook = V2();
    this.startTitle();
  }
  load(board) {
    this.initialBoard = board;
    this.board.fromJSON(board);
    for (const [, group] of this.board.sprites) {
      for (const s of group) {
        const out = this.setupSprite(s);
        this.board.resetSprite(out);
      }
    }
  }
  static whiteNoise(x, y=0, z=0, w=0) {
    return (Math.sin(12.9898 * x + 78.233 * y + 17.3392 * z + 47.743 * w) * 43758.5453) % 1;
  }
  save() {
    return JSON.stringify(this.board.toJSON());
  }
  quit() {
    this.startTitle();
  }
  toggleHitboxes() {
    this.showHitboxes = !this.showHitboxes;
  }
  player() {
    return this.board.player();
  }
  centerCamera(pos) {
    return pos.clone().subV2(SCREEN_SIZE.clone().mulScalar(0.5));
  }
  advancePlayer() {
    const {board} = this;
    const player = board.player();
    if (!player) return player;
    if (player.invis > 0) {
      player.invis -= 1;
    }
    const PLAYER_ACCEL = 1/8;
    const move = V2(this.joyMove.x || this.xmove[0] || 0,
                    this.joyMove.y || this.ymove[0] || 0);
    const moveLength = move.length();
    if (moveLength > 1) move.divScalar(moveLength);
    move.mulScalar(player.speed);
    if (moveLength === 0) {
      if (this.moving) {
        this.moving = false;
        this.slowTimer = THRUST_DECAY_TICKS + 1;
      }
      player.vel.mulScalar(1 - PLAYER_ACCEL);
    } else {
      if (!this.moving) {
        this.moving = true;
        this.playSound('thrust', { loop: true, volume: 0.25 });
      }
      const particleParams = {
        size: V2(0, 0),
        vel: move.clone().mulScalar(-1).rotate(randRange(this.random)(-0.2, 0.2)),
        velVariance: 1/8,
        counts: [1, 2]
      };
      this.spawnParticles({
        pos: player.pos.clone().addV2(V2(-3, -4).rotate(player.rot)),
        ...particleParams
      });
      this.spawnParticles({
        pos: player.pos.clone().addV2(V2(-3, 4).rotate(player.rot)),
        ...particleParams
      });
      player.vel.addV2(move.clone().mulScalar(PLAYER_ACCEL));
    }
    const pl = player.vel.length();
    if (pl > player.speed) {
      player.vel.divScalar(pl).mulScalar(player.speed);
    }
    board.moveSprite(player, player.pos.clone().addV2(player.vel));
    const colliders = [...board.spritesIn(hitbox(player))];
    const blockers = colliders.filter(s => s !== player && blocking(player, s));
    for (const blocker of blockers) {
      if (player.vel.x || player.vel.y) {
        const circ = hitbox(blocker);
        const offset = player.pos.clone().subV2(circ.center);
        player.vel = offset.normalize().mulScalar(player.vel.length());
      }
      player.vel.addV2(blocker.vel.clone().mulScalar(8));
      board.moveSprite(player, player.pos.clone().addV2(player.vel));
      if (blocker.type === 'meatball') {
        this.playSound('flesh');
      } else {
        this.playSound('metal');
      }
      if (player.invis === 0) {
        if (blocker.type === 'enemy' || blocker.type === 'boss') {
          this.damage(blocker);
        }
        this.damage(player);
      }
    }
    const powerups = colliders.filter(s => s.type === 'powerup');
    for (const powerup of powerups) {
      this.collectPowerup(player, powerup);
    }
    this.camera = this.centerCamera(player.pos);
    if (this.lookOffset && (this.lookOffset.x || this.lookOffset.y)) {
      this.camera.addV2(this.lookOffset);
      const lastRot = player.rot;
      player.rot = this.lookOffset.angle;
      player.rvel = player.rot - lastRot;
    }
    if (this.firing) {
      if (this.firingTimer === 0) {
        this.playSound('shoot');
        if (player.weapon === 0 || player.weapon === 2) {
          this.createSprite(player.pos.clone().addV2(V2(6, 0).rotate(player.rot)),
                            'bullet', {
            rot: player.rot,
            vel: V2(5, 0).rotate(player.rot),
            parent: player
          });
        }
        if (player.weapon === 1 || player.weapon === 2) {
          const a = player.weapon === 1 ? 0.05 : 0.1;
          this.createSprite(player.pos.clone().addV2(V2(4, -6).rotate(player.rot)),
                            'bullet', {
            rot: player.rot,
            vel: V2(5, 0).rotate(player.rot - a),
            parent: player
          });
          this.createSprite(player.pos.clone().addV2(V2(4, 6).rotate(player.rot)),
                            'bullet', {
            rot: player.rot,
            vel: V2(5, 0).rotate(player.rot + a),
            parent: player
          });
        }
      }
    }
    if (this.firing || this.firingTimer) {
      this.firingTimer += 1;
      if (this.firingTimer === 6) {
        this.firingTimer = 0;
      }
    }
    this.lastPlayerPos = player.pos.clone();
    return player;
  }
  collectPowerup(player, powerup) {
    if (player.powerMeter === 0) {
      if (player.weapon < 2) {
        player.powerMeter = 1;
      } else if (player.speed < 4) {
        player.powerMeter = 2;
      } else if (player.shield < 2) {
        player.powerMeter = 3;
      } else {
        player.powerMeter = 4;
      }
    } else if (player.powerMeter === 1) {
      if (player.speed < 4) {
        player.powerMeter = 2;
      } else if (player.shield < 2) {
        player.powerMeter = 3;
      } else {
        player.powerMeter = 4;
      }
    } else if (player.powerMeter === 2) {
      if (player.shield < 2) {
        player.powerMeter = 3;
      } else {
        player.powerMeter = 4;
      }
    } else if (player.powerMeter === 3) {
      player.powerMeter = 4;
    }
    this.spawnParticles({
      pos: powerup.pos.clone().sub(8, 9),
      size: V2(13, 16),
      velRange: V2(1, 1)
    });
    this.playSound('powerup2');
    this.deleteSprite(powerup);
  }
  usePowerup() {
    const player = this.player();
    if (!player) return;
    if (player.powerMeter === 0) return;
    this.playSound('powerup1');
    if (player.powerMeter === 1) {
      player.weapon += 1;
    } else if (player.powerMeter === 2) {
      player.speed += 0.5;
    } else if (player.powerMeter === 3) {
      player.shield += 1;
    } else if (player.powerMeter === 4) {
      player.hp = 12;
    }
    player.powerMeter = 0;
  }
  spawnExplosion(pos, {scale = 1, volume = 0.5}) {
    if (volume > 0) {
      this.playSound(`explode${randInt(this.random)(1, 4)}`, {volume});
    }
    return this.createSprite(pos, 'explosion', {scale});
  }
  advanceBullet(bullet) {
    const {board} = this;
    const shape = hitbox(bullet);
    board.moveSprite(bullet, bullet.pos.clone().addV2(bullet.vel));
    const colliders = [...board.spritesIn(shape)];
    const SHIPS = ['enemy', 'boss', 'player'];
    const ship = colliders.find(s => s !== bullet.parent && SHIPS.includes(s.type));
    if (ship || colliders.some(s => s !== bullet.parent && blocking(bullet, s))) {
      board.deleteSprite(bullet);
      if (ship) {
        this.damage(ship);
        if (bullet.parent.type === 'player') {
          if (ship.type === 'boss') {
            ship.anger += 1;
          }
          if (ship.hp === 0) {
            bullet.parent.score += SPRITES[ship.type].value || 0;
            this.highScore = Math.max(bullet.parent.score, this.highScore);
          } else if (ship.type === 'boss' && randInt(this.random)(0, 64) === 0) {
            ship.target = bullet.parent;
          }
        }
      }
      if (!ship || ship.hp > 0 || ship.type === 'boss') {
        this.spawnExplosion(bullet.pos, {scale: randRange(this.random)(0.25, 0.4)});
      }
    } else if (!shape.intersects(this.cameraRect)) {
      board.deleteSprite(bullet);
    }
  }
  advanceEnemy(sprite) {
    const SPEED = 2;
    const CHASE_ROT_SPEED = 0.6;
    const VISION_RANGE = 512;
    const FIRING_FOV = 0.5;
    const NEIGHBOR_DELTAS = deepFreeze(
      [V2(-1, 0), V2(1, 0), V2(0, -1), V2(0, 1),
       V2(-1, -1), V2(1, -1), V2(-1, 1), V2(1, 1)].map(v => v.clone().mulScalar(CELL_SIZE)));

    const {board} = this;

    const player = this.player();
    if (!sprite.target) {
      sprite.target = player;
    }
    if (!player && sprite.target) {
      sprite.target = null;
    }
    if (sprite.target) {
      const poffset = sprite.target.pos.clone().subV2(sprite.pos);
      if (poffset) {
        const pl = poffset.length();
        if (pl >= VISION_RANGE) {
          sprite.target = null;
        }
      }
    }
    const colliders = [...board.spritesIn(hitbox(sprite))];
    const blockers = colliders.filter(s => s !== sprite && blocking(sprite, s));
    if (blockers.length) {
      this.deleteSprite(sprite);
      return;
    }
    if (sprite.target) {
      const coffset = sprite.target.pos.clone().subV2(sprite.pos);
      const a = normalizeAngle(coffset.angle);
      sprite.rot = a;
      if (sprite.firingTimer === 0) {
        const dist = sprite.pos.clone().subV2(this.lastPlayerPos).length();
        this.playSound('enemy-shoot', {volume: (SOUND_THRESHOLD - dist) / SOUND_THRESHOLD * 0.5});
        this.createSprite(sprite.pos.clone().addV2(V2(4, -11).rotate(sprite.rot)),
                          'bullet', {
          rot: sprite.rot,
          vel: V2(4.5, 0).rotate(sprite.rot),
          parent: sprite,
          skin: 'enemy'
        });
      }
      if (sprite.firingTimer === 8) {
        const dist = sprite.pos.clone().subV2(this.lastPlayerPos).length();
        this.playSound('enemy-shoot', {volume: (SOUND_THRESHOLD - dist) / SOUND_THRESHOLD * 0.5});
        this.createSprite(sprite.pos.clone().addV2(V2(4, 11).rotate(sprite.rot)),
                          'bullet', {
          rot: sprite.rot,
          vel: V2(4.5, 0).rotate(sprite.rot),
          parent: sprite,
          skin: 'enemy'
        });
      }
    }
    sprite.firingTimer += 1;
    if (sprite.firingTimer === 16) {
      sprite.firingTimer = 0;
    }
    if (sprite.turnTarget !== null && Math.abs(sprite.rot - sprite.turnTarget) < CHASE_ROT_SPEED) {
      sprite.rot = sprite.turnTarget;
      sprite.rvel = 0;
      sprite.turnTarget = null;
    }
    sprite.rot = normalizeAngle(sprite.rvel + sprite.rot);
    /*

    const blocked = s => s.type !== 'player' && blocking(sprite, s);

    let targetPos = sprite.pos.clone().addV2(V2(1, 0).rotate(sprite.rot).mulScalar(CELL_SIZE));
    let targetAngle = null;
    const target = board.cell(targetPos);

    if (target && target.sprites.some(blocked)) {
      if (sprite.turnTarget === null) {
        const cells = NEIGHBOR_DELTAS
          .filter(p => {
            const cell = board.cell(p);
            return cell && !cell.sprites.some(blocked);
          });
        if (cells.length) {
          const cell = randItem(this.random)(cells);
          targetPos = cell.pos;
        } else {
          sprite.vel.mulScalar(0.5);
          sprite.rvel = 0;
          targetPos = null;
        }
      } else {
        sprite.vel.mulScalar(0.5);
        sprite.rvel = 0;
        targetPos = null;
      }
    }
    if (!sprite.target) {
      sprite.target = this.player();
    }
    if (sprite.target) {
      const poffset = sprite.target.pos.clone().subV2(sprite.pos);
      if (poffset) {
        const pl = poffset.length();
        if (pl >= VISION_RANGE) {
          sprite.target = null;
        } else {
          const noffset = poffset.normalize().mulScalar(32);
          const spos = sprite.pos.clone();
          // draw line of sight
          let sightClear = true;
          for (let off = 0; off < pl; off += 32) {
            const cell = board.cell(spos);
            if (cell && cell.sprites.some(blocked)) {
              sightClear = false;
              sprite.target = null;
              break;
            }
            spos.addV2(noffset);
          }
          if (sightClear) {
            targetPos = sprite.target.pos.clone();
          }
        }
      }
    }
    if (targetPos) {
      const coffset = targetPos.clone().subV2(sprite.pos);
      const a = normalizeAngle(coffset.angle + Math.PI / 2);
      const dist = normalizeAngle(a - sprite.rot);
      if (Math.abs(dist) >= CHASE_ROT_SPEED) {
        sprite.rvel = Math.sign(dist) * CHASE_ROT_SPEED;
        sprite.turnTarget = a;
      }
      if (sprite.target && Math.abs(dist) < FIRING_FOV) {
        if (sprite.firingTimer === 0) {
          const dist = sprite.pos.clone().subV2(this.lastPlayerPos).length();
          this.playSound('enemy-shoot', {volume: (SOUND_THRESHOLD - dist) / SOUND_THRESHOLD * 0.5});
          this.createSprite(sprite.pos.clone().addV2(V2(4, -11).rotate(sprite.rot)),
                            'bullet', {
            rot: sprite.rot,
            vel: V2(5, 0).rotate(sprite.rot),
            parent: sprite,
            skin: 'enemy'
          });
          this.createSprite(sprite.pos.clone().addV2(V2(4, 11).rotate(sprite.rot)),
                            'bullet', {
            rot: sprite.rot,
            vel: V2(5, 0).rotate(sprite.rot),
            parent: sprite,
            skin: 'enemy'
          });
        }
      }
    }
    if (this.firingTimer) {
      this.firingTimer += 1;
      if (this.firingTimer === 6) {
        this.firingTimer = 0;
      }
    }
    if (sprite.turnTarget !== null && Math.abs(sprite.rot - sprite.turnTarget) < CHASE_ROT_SPEED) {
      sprite.rot = sprite.turnTarget;
      sprite.rvel = 0;
      sprite.turnTarget = null;
    }
    sprite.rot = normalizeAngle(sprite.rvel + sprite.rot);
    sprite.vel = V2(0.1, 0).rotate(sprite.rot);
    this.board.moveSprite(sprite, sprite.pos.clone().addV2(sprite.vel));
    */
  }
  advanceBoss(sprite) {
    const bossExplosion = () => {
      const t = randRange(this.random)(0, Math.PI * 2);
      const offset = V2(0, randInt(this.random)(0, SPRITES.boss.radius)).rotate(t);
      const pos = sprite.pos.clone().addV2(offset);
      const scale = randRange(this.random)(0.75, 1);
      const expl = this.spawnExplosion(pos, {scale});
      expl.vel = offset.divScalar(SPRITES.boss.radius / 4);
      return expl;
    };
    if (sprite.hp <= 0) {
      if (sprite.deathAnim > 0) {
        sprite.rot += 0.2;
        if (sprite.deathAnim % 4 === 0) {
          const expl = bossExplosion();
          expl.vel = V2();
        }
        sprite.deathAnim -= 1;
      }
      if (sprite.deathAnim === 0) {
        for (const tail of sprite.tail) {
          this.deleteSprite(tail);
        }
        this.deleteSprite(sprite);
        for (let i = 0; i < 16; i += 1) {
          bossExplosion();
        }
        return;
      }
    } else {
      const SEARCH_SPEED = 1;
      const CHASE_SPEED = 2.9;
      const SEARCH_ROT_SPEED = 0.01;
      const CHASE_ROT_SPEED = 0.06;
      const VISION_RANGE = 176;
      const player = this.player();

      let speed = SEARCH_SPEED;
      let oldTarget = sprite.target;
      if (!sprite.target) {
        sprite.target = player;
      }
      const poffset = sprite.target && sprite.target.pos.clone().subV2(sprite.pos);
      if (!poffset || poffset.length() >= VISION_RANGE + sprite.anger * 2) {
        sprite.target = null;
      }
      if (sprite.target && sprite.target.hp <= 0) {
        sprite.target = null;
      }
      if (sprite.target) {
        speed = Math.min(sprite.target.speed - 0.1, CHASE_SPEED);
        const a = poffset.angle + Math.PI / 2;
        const dist = normalizeAngle(a - sprite.rot);
        if (Math.abs(dist) >= CHASE_ROT_SPEED) {
          sprite.rvel = Math.sign(dist) * CHASE_ROT_SPEED;
        } else {
          sprite.rvel = 0;
          sprite.rot = a;
        }
        sprite.turnTimer = 0;
      } else {
        if (!sprite.turnTimer && randInt(this.random)(0, 50) === 0) {
          sprite.rvel = (randInt(this.random)(0, 2) * 2 - 1) * SEARCH_ROT_SPEED;
          sprite.turnTimer = randInt(this.random)(60, 120);
        } else if (sprite.turnTimer > 0) {
          sprite.turnTimer -= 1;
        }
        if (sprite.turnTimer === 0) {
          sprite.rvel = 0;
        }
      }
      sprite.rot += sprite.rvel;
      sprite.vel = V2(0, -speed).rotate(sprite.rot);
      this.board.moveSprite(sprite, sprite.pos.clone().addV2(sprite.vel));
      const colliders = [...this.board.spritesIn(hitbox(sprite))];
      const blocker = colliders.find(s => s !== sprite && s.type === 'boss');
      if (blocker) {
        const offset = sprite.pos.clone().subV2(blocker.pos);
        const ol = offset.length();
        const mag = SPRITES.boss.radius * 2 - ol;
        const move = offset.divScalar(ol).mulScalar(mag);
        this.board.moveSprite(sprite, sprite.pos.clone().addV2(move));
      }
    }
    let target = sprite;
    for (let i = 0; i < sprite.tail.length; i += 1) {
      const tail = sprite.tail[i];
      const offset = target.pos.clone().subV2(tail.pos);
      if (offset.length() >= 256 - i * 8) {
        tail.vel = offset.normalize().mulScalar(sprite.vel.length());
      } else {
        tail.vel = offset.mulScalar(1/16);
      }
      this.board.moveSprite(tail, tail.pos.clone().addV2(tail.vel));
      target = tail;
    }
  }
  advancePowerup(sprite) {
    let colliders = [...this.board.spritesIn(hitbox(sprite))];
    if (colliders.some(s => s.type === 'meatball')) {
      this.deleteSprite(sprite);
      return;
    }
    sprite.anim += 1;
    const PHASE = 90;
    if (sprite.anim === PHASE) {
      sprite.anim = 0;
    }
    const delta = Math.sin(sprite.anim / PHASE * Math.PI * 2) * 6;
    sprite.pos.y = sprite.initial.pos.y + delta;
    colliders = [...this.board.spritesIn(hitbox(sprite))];
    if (colliders.some(s => s.type === 'meatball')) {
      sprite.initial.pos.y -= delta;
      sprite.pos.y -= delta;
    }
  }
  damage(sprite) {
    if (sprite.type === 'player' && sprite.invis > 0 && sprite.hp > 0) {
      return;
    }
    sprite.hp -= 1;
    if (sprite.type === 'player') {
      sprite.invis = sprite.shield * 60;
      this.cameraShake += 0.25;
    }
    if (sprite.hp <= 0) {
      if (sprite.type === 'boss') {
        if (sprite.deathAnim === 0) {
          sprite.deathAnim = 60*3;
        }
        return;
      }
      const props = { volume: 0.5, scale: 0.5 };
      let scale = 0.5;
      this.deleteSprite(sprite);
      if (sprite.type === 'player') {
        props.volume = 0.75;
        props.scale = 1;
        this.cameraShake += 0.25;
        this.cameraDecel = sprite.vel.clone();
        this.gameOverTimer = 120;
        this.finalScore = sprite.score;
        if (sprite.vel.x || sprite.vel.y) {
          this.slowTimer = THRUST_DECAY_TICKS + 1;
        }
      }
      this.spawnExplosion(sprite.pos, props);
    }
  }
  playSound(sound, opts = {}) {
    const s = this.sounds[sound];
    if (s) {
      const {volume = 1} = opts;
      if (opts.channel === 'music') {
        opts.volume = volume * this.config.musicVolume;
      } else {
        opts.volume = volume * this.config.sfxVolume;
      }
      Sound.trigger(this, s, opts);
    }
    if (opts.channel) {
      const oldSound = this.soundChannels[opts.channel];
      if (oldSound) {
        Sound.stop(oldSound);
      }
      if (s) {
        this.soundChannels[opts.channel] = s;
      } else {
        delete this.soundChannels[opts.channel];
      }
    }
  }
  stopSound(sound) {
    const s = this.sounds[sound];
    if (s) {
      Sound.stop(s);
    }
  }
  setSoundVolume(sound, volume) {
    const s = this.sounds[sound];
    if (s) {
      Sound.setVolume(s, volume);
    }
  }
  spawnParticles({pos, size, vel=V2(0, 0), velRange=V2(0, 0), velVariance=0, counts=[8, 15]}) {
    const particles = randInt(this.random)(...counts);
    const added = [];
    for (let i = 0; i < particles; i += 1) {
      const xo = this.random() * size.x;
      const yo = this.random() * size.y;
      const vfactor = 1 - velVariance;
      const vx = [(vel.x - velRange.x) * vfactor, (vel.x + velRange.x) / vfactor];
      const vy = [(vel.y - velRange.y) * vfactor, (vel.y + velRange.y) / vfactor];
      if (vel.x < 0) vx.reverse();
      if (vel.y < 0) vy.reverse();
      added.push(this.createSprite(pos.clone().add(xo, yo), 'particle', {
        vel: V2(randRange(this.random)(...vx),
                randRange(this.random)(...vy)),
        lifetime: 24 + randRange(this.random)(-6, 6)
      }));
    }
    return added;
  }
  checkThrust() {
    if (this.slowTimer) {
      this.slowTimer -= 1;
      if (this.slowTimer > 0) {
        this.setSoundVolume('thrust', this.slowTimer / THRUST_DECAY_TICKS * 0.125);
      } else {
        this.stopSound('thrust');
      }
    }
  }
  advance() {
    const {board} = this;

    const player = this.advancePlayer();
    this.checkThrust();

    if (!player) {
      if (this.cameraDecel) {
        this.camera.addV2(this.cameraDecel);
        this.cameraDecel.mulScalar(1 - 1/16);
      }
      if (this.gameOverTimer > 0) {
        this.gameOverTimer -= 1;
      }
    }

    const CAMERA_RECT_BUFFER = CELL_SIZE * 5;
    this.cameraRect = AR2(
      this.lastPlayerPos.x - CAMERA_RECT_BUFFER - this.canvas.width / 2,
      this.lastPlayerPos.y - CAMERA_RECT_BUFFER - this.canvas.height / 2,
      this.canvas.width + CAMERA_RECT_BUFFER * 2,
      this.canvas.height + CAMERA_RECT_BUFFER * 2);

    const gridA = this.cameraRect.topLeft.divScalar(GRID_SPACING).map(v => Math.floor(v));
    const gridB = this.cameraRect.bottomRight.divScalar(GRID_SPACING).map(v => Math.ceil(v));
    for (let y = gridA.y; y < gridB.y; y += 1) {
      for (let x = gridA.x; x < gridB.x; x += 1) {
        const key = `${x},${y}`;
        if (!this.createdTiles.has(key)) {
          this.genTile(V2(x, y));
          this.createdTiles.add(key);
        }
      }
    }

    let enemyThrust = 0;
    let bossThrust = 0;
    for (const sprite of [...this.board.spritesIn(this.cameraRect)]) {
      if (sprite.type === 'enemy') {
        this.advanceEnemy(sprite);
        const dist = sprite.pos.clone().subV2(this.lastPlayerPos).length();
        enemyThrust = Math.max(enemyThrust, SOUND_THRESHOLD - dist);
      }
      if (sprite.type === 'boss') {
        this.advanceBoss(sprite);
        const dist = sprite.pos.clone().subV2(this.lastPlayerPos).length();
        bossThrust = Math.max(bossThrust, SOUND_THRESHOLD - dist);
      }
      if (sprite.type === 'bullet') {
        this.advanceBullet(sprite);
      }
    }
    for (const sprite of [...board.sprites.get('bullet')]) {
      if (!hitbox(sprite).intersects(this.cameraRect)) {
        board.deleteSprite(sprite);
      }
    }
    this.setSoundVolume('enemy', enemyThrust / SOUND_THRESHOLD * 0.15);
    this.setSoundVolume('boss', bossThrust / SOUND_THRESHOLD * 0.35);
    for (const sprite of [...board.sprites.get('powerup')]) {
      this.advancePowerup(sprite);
    }
    for (const type of ['particle', 'explosion']) {
      for (const particle of [...board.sprites.get(type)]) {
        particle.anim += 1;
        if (particle.anim >= (particle.lifetime || SPRITES[particle.type].lifetime)) {
          board.deleteSprite(particle);
        } else {
          particle.pos.addV2(particle.vel);
        }
      }
    }
  }
  pause() {
    this.playSound('blip');
    this.mode = 'pause';
    this.setSoundVolume('thrust', 0);
    this.setSoundVolume('enemy', 0);
    this.setSoundVolume('boss', 0);
  }
  startTitle() {
    this.cameraShake = 0;
    this.timer = 0;
    this.camera = V2();
    this.mode = 'title';
    if (this.sounds) {
      this.stopSound('boss');
      this.stopSound('enemy');
    }
    this.playSound('theme', { loop: true });
  }
  start() {
    this.playSound('blip');
    this.mode = 'game';
    this.camera = V2();
    this.lookOffset = null;
    this.startTime = null;
    this.joyLook = V2();
    this.joyMove = V2();
    this.xmove = [];
    this.ymove = [];
    this.touches = new Map;
    this.timer = 0;
    this.firing = false;
    this.firingTimer = 0;
    for (const button of Object.values(this.touchButtons)) {
      button.alpha = 0;
      button.pressed = false;
    }
    this.board.clear();
    this.createSprite(V2(0, 0), 'player');
    this.mapSeed = this.random();
    this.createdTiles = new Set;
    this.stopSound('theme');
    this.playSound('enemy', { loop: true, volume: 0 });
    this.playSound('boss', { loop: true, volume: 0 });
  }
  genTile(gridPos) {
    const BOSS_SPAWN_DIST = 16 * GRID_SPACING;
    const ENEMY_SPAWN_DIST = 4 * GRID_SPACING;

    if (gridPos.y <= 1 && gridPos.y >= -1 && gridPos.x <= 1 && gridPos.x >= -1) return;
    const xo = Game.whiteNoise(gridPos.x, gridPos.y, this.mapSeed, 0.54326);
    const yo = Game.whiteNoise(gridPos.x, gridPos.y, this.mapSeed, 0.36142);
    const pos = gridPos.clone().mulScalar(GRID_SPACING)
      .add(xo * 32, yo * 32);
    const dist = pos.length();
    const n = Game.whiteNoise(gridPos.x, gridPos.y, this.mapSeed);
    const r = (n + 1) * (100 / 2);
    if (r >= 90) {
      this.createSprite(pos, 'meatball');
    } else if (r >= 80) {
      this.createSprite(pos, 'meatball', {scale: 0.5});
    } else if (r >= 70) {
      this.createSprite(pos, 'meatball', {scale: 0.25});
    } else if (r >= 65) {
      const sprite = this.createSprite(pos, 'powerup');
    } else if (r >= 55 && dist >= ENEMY_SPAWN_DIST) {
      this.createSprite(pos, 'enemy');
    } else if (r >= 54.5 && dist >= BOSS_SPAWN_DIST) {
      this.createSprite(pos, 'boss');
    }
  }
  mouseLook({clientX, clientY}) {
    this.playerCameraLook(
      this.screenNormCoord(V2(clientX, clientY))
      .subScalar(0.5)
      .mulScalar(2));
  }
  playerCameraLook(pos) {
    const player = this.player();
    if (player) {
      const bounds = SCREEN_SIZE.clone().mulScalar(0.25);
      this.lookOffset = pos.mulV2(bounds);
      /*
      this.lookOffset.x = clamp(this.lookOffset.x, -bounds.x, bounds.x);
      this.lookOffset.y = clamp(this.lookOffset.y, -bounds.y, bounds.y);
      */
    }
  }
  stopFire() {
    this.firing = false;
  }
  stopMouseLook() {
    this.stopFire();
  }
  touchStart({changedTouches}) {
    for (let i = 0; i < changedTouches.length; i += 1) {
      const touch = changedTouches.item(i);
      const touchPos = this.screenCoord(V2(touch.clientX, touch.clientY));
      this.touches.set(touch.identifier, touch);
      const button = checkTouch(touchPos, this.touchButtons);
      if (button) {
        touch.button = button;
        button.press();
        button.pressed = true;
        button.alpha = 1;
      } else {
        this.mouseLook(touch);
      }
    }
  }
  touchMove({changedTouches}) {
    for (let i = 0; i < changedTouches.length; i += 1) {
      const touch = changedTouches.item(i);
      const oldTouch = this.touches.get(touch.identifier);
      // const touchPos = this.screenCoord(V2(touch.clientX, touch.clientY));
      this.touches.set(touch.identifier, touch);
      if (oldTouch && oldTouch.button) {
        touch.button = oldTouch.button;
      } else {
        this.mouseLook(touch);
      }
    }
  }
  touchEnd({changedTouches}) {
    let lookCount = 0;
    for (let i = 0; i < changedTouches.length; i += 1) {
      const touch = changedTouches.item(i);
      const oldTouch = this.touches.get(touch.identifier);
      this.touches.delete(touch.identifier);
      if (oldTouch && oldTouch.button) {
        oldTouch.button.release();
        oldTouch.button.pressed = false;
      } else {
        lookCount += 1;
      }
    }
    if (lookCount && [...this.touches.values()].every(t => t.button)) {
      this.stopMouseLook();
    }
  }
  fire() {
    this.firing = true;
  }
  tickPlay(events) {
    if (!this.startTime) this.startTime = this.window.performance.now();
    for (const e of events) {
      if (e.type === 'keydown') {
        const player = this.board.player();
        if (player) {
          (this.keybinds[e.code] || (() => {}))();
        } else if (this.gameOverTimer === 0) {
          this.startTitle();
        }
      } else if (e.type === 'keyup') {
        (this.keyups[e.code] || (() => {}))();
      } else if (e.type === 'mousedown' && e.button === 0) {
        if (!this.system.touchEnabled) {
          this.fire();
        }
      } else if (e.type === 'mousedown' && e.button === 2) {
        if (!this.system.touchEnabled) {
          this.usePowerup();
        }
      } else if (e.type === 'mouseup' && (e.button === 0)) {
        if (!this.system.touchEnabled) {
          this.stopMouseLook();
        }
      } else if (e.type === 'mousemove') {
        if (!this.system.touchEnabled) {
          this.mouseLook(e);
        }
      } else if (e.type === 'mousemove' && !(e.buttons & 1)) {
        if (!this.system.touchEnabled) {
          this.stopMouseLook();
        }
      } else if (e.type === 'movex') {
        this.joyMove.x = e.value;
      } else if (e.type === 'movey') {
        this.joyMove.y = e.value;
      } else if (e.type === 'lookx') {
        this.joyLook.x = e.value;
      } else if (e.type === 'looky') {
        this.joyLook.y = e.value;
      } else if (e.type === 'touchstart' && e.changedTouches.length) {
        this.touchStart(e);
      } else if (e.type === 'touchmove' && e.changedTouches.length) {
        this.touchMove(e);
      } else if (e.type === 'touchend' && e.changedTouches.length) {
        this.touchEnd(e);
      }
    }
    for (const button of Object.values(this.touchButtons)) {
      if (!button.pressed && button.alpha > 0) {
        button.alpha -= 1/8;
      }
    }
    if (this.joyLook.length() > 0.01) {
      this.playerCameraLook(this.joyLook.clone());
    }
    if (this.mode === 'game') this.advance();
  }
  tickSound() {
    const {config, soundChannels} = this;
    for (const [name, channel] of Object.entries(soundChannels)) {
      const volume = name === 'music' ? config.musicVolume : config.sfxVolume;
      channel.gainNode.gain.value = volume;
    }
  }
  tick() {
    this.tickSound();
    if (this.mode !== 'pause') {
      this.globalTimer += 1;
    }
    this.now = window.performance.now();
    const events = this.system.poll();
    if (this.mode === 'title') {
      this.timer += 1;
      if (this.timer > 90) {
        this.timer = 0;
      }
      if (this.random() < 1/40) {
        this.cameraShake = TICK_LENGTH / 1000 * 2;
      }
      for (const e of events) {
        if ((e.type === 'keydown' && !INVALID_KEYS.has(e.code)) || e.type === 'mousedown') {
          this.start();
        }
      }
    } else if (this.mode === 'game') {
      this.tickPlay(events);
    } else if (this.mode === 'pause') {
      for (const e of events) {
        if (e.type === 'keydown' && e.code === 'Enter') {
          this.playSound('blip');
          this.mode = 'game';
        }
      }
    }
  }
  createSprite(pos, type, props = {}) {
    const sprite = this.board.createSprite(pos.clone(), type);
    if (SPRITES[type].particle) sprite.anim = 0;
    for (const prop of SPRITES[type].props || []) {
      sprite.initial[prop] = props[prop];
    }
    return Object.assign(this.setupSprite(sprite), props);
  }
  deleteSprite(sprite) {
    this.board.deleteSprite(sprite);
  }
  setupSprite(sprite) {
    const {setup = (() => {})} = SPRITES[sprite.type];
    return Object.assign(sprite, setup(sprite, this));
  }
  canvasRect() {
    let { top, left, width, height } = this.canvas.getBoundingClientRect(); // eslint-disable-line prefer-const
    const ratio = this.canvas.width / this.canvas.height;
    if (width / height > ratio) {
      left += (width - height * ratio) / 2;
      width = height * ratio;
    } else {
      top += (height - width * (1/ratio)) / 2;
      height = width * (1/ratio);
    }
    return { top, left, width, height };
  }
  projectNormCoord(pos) {
    const { top, left, width, height } = this.canvasRect();
    return pos.clone()
      .mul(width, height)
      .add(left, top);
  }
  projectCoord(pos) {
      return this.projectNormCoord(
        pos.clone().div(this.canvas.width, this.canvas.height));
  }
  screenNormCoord(pos) {
    const { top, left, width, height } = this.canvasRect();
    return pos.clone()
      .sub(left, top)
      .div(width, height);
  }
  screenCoord(pos) {
      return this.screenNormCoord(pos)
        .mul(this.canvas.width, this.canvas.height);
  }
  worldCoord(pos) {
      return this.screenCoord(pos)
        .addV2(this.camera);
  }
}

