import ALEA_LICENSE from './util/alea.LICENSE';
import GL_MATRIX_LICENSE from '../node_modules/gl-matrix/LICENSE.md';
import JS_UNTAR_LICENSE from '../node_modules/js-untar/LICENSE.txt';
import LOCALFORAGE_LICENSE from '../node_modules/localforage/LICENSE';
import POLYFILL_LICENSE from '../node_modules/@babel/polyfill/LICENSE';
import RUNTIME_LICENSE from '../node_modules/@babel/runtime/LICENSE';
import SIMPLEX_LICENSE from './util/simplex.LICENSE';
import V2 from './util/vector2.js';
import {createElement} from './framework.js';

export default function createCredits({ui}) {
  return ui.bus.tag('creditsToggled', () => ui.credits
? <section class="dialog credits" style={Object.assign(ui.project(V2()).toStyle(), ui.project(V2()).toStyle('right', 'bottom'))}>
    <header><span>Credits</span><button class="close-button" onClick={ui.closeCredits}></button></header>
    <article class="dialog-scroll">
      <h3>Game by <a href="https://github.com/cyclopsian">cyclopsian</a></h3>
      <p>Code licensed under the <a href="https://www.gnu.org/licenses/gpl-3.0-standalone.html">GNU GPLv3</a>.<br />
         Art and sound licensed under <a href ="https://creativecommons.org/licenses/by-sa/4.0/">CC-BY-SA 4.0</a>.</p>
      <h3>Third-Party Libraries</h3>
      <h4><a href="https://github.com/babel/babel/tree/master/packages/babel-polyfill">@babel/polyfill</a></h4>
      <pre>{POLYFILL_LICENSE}</pre>
      <h4><a href="https://github.com/babel/babel/tree/master/packages/babel-runtime">@babel/runtime</a></h4>
      <pre>{RUNTIME_LICENSE}</pre>
      <h4><a href="https://github.com/toji/gl-matrix">gl-matrix</a></h4>
      <pre>{GL_MATRIX_LICENSE}</pre>
      <h4><a href="https://github.com/InvokIT/js-untar">js-untar</a></h4>
      <pre>{JS_UNTAR_LICENSE}</pre>
      <h4><a href="https://localforage.github.io/localForage/">localForage</a></h4>
      <pre>{LOCALFORAGE_LICENSE}</pre>
      <h4><a href="https://github.com/jwagner/simplex-noise.js">simplex-noise.js</a></h4>
      <pre>{SIMPLEX_LICENSE}</pre>
      <h4><a href="https://github.com/coverslide/node-alea">alea</a></h4>
      <pre>{ALEA_LICENSE}</pre>
      <h3>Special Thanks</h3>
      <p>The <a href="https://webpack.js.org/" title="Webpack">Webpack</a> project and its contributors</p>
      <p>The <a href="https://babeljs.io/" title="Babel">Babel</a> project and its contributors</p>
      <h3>Third-Party Art</h3>
      <p>Icons credited to <a href="https://twitter.com/keyamoon" title="Keyamoon" target="_blank">Keyamoon</a> at <a href="https://icomoon.io" title="Icomoon" target="_blank">Icomoon</a>, licensed under <a href="http://creativecommons.org/licenses/by/4.0/" title="Creative Commons BY 4.0" target="_blank">CC 4.0 BY</a></p>
    </article>
  </section> : null);
}

