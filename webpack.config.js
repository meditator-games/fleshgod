/* eslint-env node */
const path = require('path');
const webpack = require('webpack');
const babelOptions = require('./babel.config.js')();
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = env => ({
  entry: ['./src/bootstrap.js'],
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js'
  },
  mode: env === 'production' ? env : 'development',
  target: 'web',
  devtool: env === 'production' ? false : 'source-map',
  resolve: {
    alias: {
      atlases$: path.resolve(__dirname, '.build/atlases.json'),
    }
  },
  module: {
    rules: [
    {
      test: /\.jsx?$/,
      include: [path.resolve(__dirname, 'src')],
      use: [{
        loader: 'babel-loader',
        options: Object.assign({
          cacheDirectory: true
        }, babelOptions)
      }]
    },
    {
      test: /\.(frag|vert)$/,
      use: 'raw-loader'
    },
    {
      test: /.*LICENSE.*/,
      use: 'raw-loader'
    },
    {
      test: /\.html$/,
      include: [path.resolve(__dirname, 'public')],
      loader: 'html-loader'
    },
    {
      test: /.js$/,
      sideEffects: false
    }
    ]
  },
  plugins: [...(env === 'production' ? [
    new UglifyJsPlugin({
      uglifyOptions: {
        sourceMap: true,
        ecma: 8,
        compress: true,
        mangle: true
      }
    }),
    new webpack.LoaderOptionsPlugin({ minimize: true })
  ] : []), ...[
    new webpack.DefinePlugin({PRODUCTION: env === 'production'}),
    new HtmlWebpackPlugin({
      minify: env === 'production' && {
        minifyCSS: true,
        collapseWhitespace: true
      },
      template: 'src/index.html'
    })
  ]],
  optimization: {
    splitChunks: {
      chunks: 'all',
      cacheGroups: {
        webgl: {
          name: 'webgl',
          test: /[\\/]node_modules[\\/]gl-matrix[\\/]/
        }
      }
    }
  },
  devServer: {
    contentBase: [path.join(__dirname, 'dist'), path.join(__dirname, 'public')],
    compress: true,
    host: '0.0.0.0',
    port: 9000,
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': 'true',
      'Access-Control-Allow-Headers': 'Content-Type, Authorization, x-id, Content-Length, X-Requested-With',
      'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS'
    }
  }
});
