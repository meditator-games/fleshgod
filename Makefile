
APP=fleshgod
IMAGES:=$(shell find resources/gfx/ -type f)
SOUNDS:=$(shell find resources/snd/ -type f)
SVG:=$(shell find resources/ui/ -type f -name '*.svg')
JS:=$(shell find src -type f)
DISTDIR=dist
SNDARCHIVE=$(DISTDIR)/snd.tar
GFXARCHIVE=$(DISTDIR)/gfx.tar
UIDIR=$(DISTDIR)/ui
ENV?=development
ZIPARC=artifacts/$(APP).zip
TARARC=artifacts/$(APP).tar

.PHONY: $(APP) resources gfx opt_gfx snd ui zip tgz all clean clean_webpack

$(APP): dist/bundle.js $(GFXARCHIVE) $(SNDARCHIVE) $(UIDIR)
resources: gfx snd ui
gfx: $(GFXARCHIVE)
snd: $(SNDARCHIVE)
ui: dist/ui
zip: $(ZIPARC)
tgz: $(TARARC)
all: dist/bundle.js $(ZIPARC) $(TARARC)
clean:
	rm -rf $(DISTDIR) .build artifacts
clean_webpack:
	rm -f $(DISTDIR)/*.js $(DISTDIR)/*.map

dist/bundle.js: $(JS) node_modules .build/atlases.json webpack.config.js
	npx webpack --env $(ENV)

.build/gen-textures.js: src/gen-textures.js node_modules
	npx babel src/gen-textures.js -d .build

.build/atlases.json: .build/gen-textures.js $(IMAGES)
	mkdir -p .build/gfx
	node .build/gen-textures.js resources/gfx .build/gfx

opt_gfx: .buiild/atlases.json
	optipng -o5 .build/gfx/*.png

$(DISTDIR):
	mkdir -p $(DISTDIR)

$(GFXARCHIVE): .build/atlases.json $(DISTDIR)
	cd .build/gfx && tar -cf ../../$(GFXARCHIVE) *

$(SNDARCHIVE): $(SOUNDS) $(DISTDIR)
	cd resources/snd && tar -cf ../../$(SNDARCHIVE) *

$(UIDIR): $(SVG) $(DISTDIR)
	mkdir -p dist/ui
	cd resources/ui && npx svgo -o ../../$(UIDIR) -f .

$(ZIPARC): $(APP) $(DISTDIR)
	mkdir -p artifacts
	cd $(DISTDIR) && zip -r ../$(ZIPARC) .
	cd public && zip -g -r ../$(ZIPARC) .

$(TARARC): $(APP) $(DISTDIR)
	mkdir -p artifacts
	cd $(DISTDIR) && tar -cf ../$(TARARC) *
	cd public && tar -rf ../$(TARARC) *
	gzip $(TARARC)

node_modules:
	npm install
