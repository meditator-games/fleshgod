# ![Fleshgod](https://gitlab.com/meditator/fleshgod/raw/master/resources/gfx/title.png)

A bullet hell shooter with meatballs. Made with ES7 + WebGL. Fleshgod is [free software](https://www.gnu.org/philosophy/free-sw.en.html). Code is licensed under the [GNU GPLv3](https://www.gnu.org/licenses/gpl-3.0-standalone.html). Art and sound (in the resources folder) is licensed under [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).

![Screenshot](https://gitlab.com/meditator/fleshgod/raw/master/work/screenshots/shot4.png)

### Installation

To run the game locally, make sure you have [Node.js](https://nodejs.org) and [GNU make](https://www.gnu.org/software/make/) installed. Clone the project to a local folder, then run:

```shell
npm install
npm run start:dev
```

Once the server is running, visit [http://localhost:9000](http://localhost:9000) in your browser.

To test in production mode:

```shell
npm run start:prod
```

To build a zip artifact:

```shell
make zip
```

### Special Thanks

To the authors of:

- [GIMP](https://www.gimp.org/)
- [GrafX2](http://grafx2.chez.com/)
- [0CC-FamiTracker](https://github.com/HertzDevil/0CC-FamiTracker)
- [Audacity](https://www.audacityteam.org/)
